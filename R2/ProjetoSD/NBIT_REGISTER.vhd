library ieee;
use ieee.std_logic_1164.all;

entity NBIT_REGISTER is
	generic (BUS_WIDTH: positive := 32);
	port
	(
	
		-- Input ports
		clk, reset, en: in std_logic;	

		-- Output ports
		d: in std_logic_vector(BUS_WIDTH-1 downto 0);
		q: out std_logic_vector(BUS_WIDTH-1 downto 0)
	);
end entity;

architecture bhv of NBIT_REGISTER is
	subtype InternalState is std_logic_vector(BUS_WIDTH-1 downto 0);
	signal nextState, currentState: InternalState;
begin

LPE: nextState <= d;
	
	ME: process (clk, reset) is
	begin
		if reset='1' then 
			currentState <= (others=>'0'); 
		elsif rising_edge(clk) then
			if en = '1' then
				currentState <= nextState;
			end if;
		end if;
		
	end process;
	
OPL: q <= currentState;

end architecture;

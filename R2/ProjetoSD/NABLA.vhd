library ieee;
use ieee.std_logic_1164.all;

entity NABLA is
generic(BUS_WIDTH: POSITIVE := 16);
port(
	N: in std_logic_vector(BUS_WIDTH-1 downto 0);
	R: out std_logic_vector(BUS_WIDTH-1 downto 0);
	
	clk, rst, iniciar: in std_logic;
	
	ready: out std_logic
	
	--phi: out std_logic_vector(31 downto 0)
);

end entity;

architecture bhv of NABLA is

component BO is 
generic(BUS_WIDTH: POSITIVE := 32);
port(

clk, reset: in std_logic;

--DATA
N: in std_logic_vector(BUS_WIDTH-1 downto 0);
R: out std_logic_vector(BUS_WIDTH-1 downto 0);

--TEST DATA OUTPUTS
--COUNTER_OUT: out std_logic_vector(BUS_WIDTH-1 downto 0);
--FIBTH_OUT: out std_logic_vector(BUS_WIDTH-1 downto 0);
--F1, F2: out std_logic_vector(BUS_WIDTH-1 downto 0);

--STATUS COUNTER

--CONTROLE -> BO

sel_f2, en_f1, en_f2, en_fibth, sel_cond1, sel_cond2, en_count, sel_count: in std_logic;
--en_phi: in std_logic;

--B0 -> CONTROLE
comp_res, n_eq_zero, n_eq_one: out std_logic
); 

end component;

component FSM is
	port(
		-- control inputs
		clk, rst: in std_logic;
		iniciar, n_eq_one, n_eq_zero, comp_res: in std_logic;
		-- control outputs
		sel_f2, en_f1, en_f2, en_fibth, sel_cond1, sel_cond2, sel_count, en_count, ready, reset_bo: out std_logic
		--en_phi: out std_logic;
		);
end component;

signal sel_f2, en_f1, en_f2, en_fibth, sel_cond1, sel_cond2, sel_count, en_count, n_eq_one, n_eq_zero, comp_res, reset_bo: std_logic;

begin

CONTROL: FSM
				port map(clk, rst, iniciar, n_eq_one, n_eq_zero, comp_res, sel_f2, en_f1, en_f2, en_fibth, sel_cond1, sel_cond2, sel_count, en_count, ready, reset_bo);
				
DATAPATH: BO 
				generic map (BUS_WIDTH => BUS_WIDTH)
				port map(clk, reset_bo, N, R, sel_f2, en_f1, en_f2, en_fibth, sel_cond1, sel_cond2, en_count, sel_count, comp_res, n_eq_zero, n_eq_one);


end architecture;
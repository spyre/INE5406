library IEEE; 
use IEEE.std_logic_1164.all;

entity BO is

generic(BUS_WIDTH: POSITIVE := 32);
port(

clk, reset: in std_logic;

--DATA
N: in std_logic_vector(BUS_WIDTH-1 downto 0);
R: out std_logic_vector(BUS_WIDTH-1 downto 0);

--TEST DATA OUTPUTS
--COUNTER_OUT: out std_logic_vector(BUS_WIDTH-1 downto 0);
--FIBTH_OUT: out std_logic_vector(BUS_WIDTH-1 downto 0);
--F1, F2: out std_logic_vector(BUS_WIDTH-1 downto 0);

--STATUS COUNTER

--CONTROLE -> BO

sel_f2, en_f1, en_f2, en_fibth, sel_cond1, sel_cond2, en_count, sel_count: in std_logic;
--en_phi: in std_logic;

--B0 -> CONTROLE
comp_res, n_eq_zero, n_eq_one: out std_logic

);

end entity;

architecture RTL of BO is

component NBIT_REGISTER is
generic(BUS_WIDTH: POSITIVE := 32);
	port
	(
	
		-- Input ports
		clk, reset, en: in std_logic;	

		-- Output ports
		d: in std_logic_vector(BUS_WIDTH-1 downto 0);
		q: out std_logic_vector(BUS_WIDTH-1 downto 0)
	);
end component;

component COMPARATOR2X1 is 
generic(BUS_WIDTH: POSITIVE := 32);
port(A: in std_logic_vector(BUS_WIDTH-1 downto 0);
	  B: in std_logic_vector(BUS_WIDTH-1 downto 0);
	  C: out std_logic
	  );
end component;

component INCREMENT is
generic(BUS_WIDTH: POSITIVE := 32);
port(A: in std_logic_vector(BUS_WIDTH-1 downto 0);
	  B: out std_logic_vector(BUS_WIDTH-1 downto 0)
);
end component;

component MUX2X1 is
generic(BUS_WIDTH: POSITIVE := 32);
port(
	A: in std_logic_vector(BUS_WIDTH-1 downto 0);
	B: in std_logic_vector(BUS_WIDTH-1 downto 0);
	SEL: in std_logic;
	C: out std_logic_vector(BUS_WIDTH-1 downto 0)
);
end component;

component NBIT_ADDER is
	generic(BUS_WIDTH: POSITIVE := 32);
	port
	(
		-- Input ports
		A: in std_logic_vector(BUS_WIDTH - 1 downto 0);
		B: in std_logic_vector(BUS_WIDTH - 1 downto 0);
		
		-- Output ports
		C: out std_logic_vector(BUS_WIDTH - 1 downto 0)
	);

end component;
signal S_MUXF2, S_REGISTERF1, S_REGISTERF2, S_ADDER, S_REGISTERFIBTH: std_logic_vector(BUS_WIDTH-1 downto 0); --sinais do bloco de operao de phi
signal S_REGISTER_COUNT, S_MUX_COUNT, S_INCREMENT_COUNT, S_ACTIONMUX1: std_logic_vector(BUS_WIDTH-1 downto 0); --sinais do countador

begin

MUXF2: MUX2X1 generic map(BUS_WIDTH => BUS_WIDTH)
						port map (x"0001", S_REGISTERFIBTH, sel_f2, S_MUXF2);

REGISTERF1: NBIT_REGISTER generic map(BUS_WIDTH => BUS_WIDTH)
						port map(clk, reset, en_f1, S_REGISTERF2, S_REGISTERF1);

REGISTERF2: NBIT_REGISTER generic map(BUS_WIDTH => BUS_WIDTH)
						port map(clk, reset, en_f2, S_MUXF2, S_REGISTERF2);

ADDERSUPREMO: NBIT_ADDER generic map(BUS_WIDTH => BUS_WIDTH)
						port map(S_REGISTERF1, S_REGISTERF2, S_ADDER);

REGISTERFITBH: NBIT_REGISTER generic map(BUS_WIDTH =>BUS_WIDTH)
						port map(clk, reset, en_fibth, S_ADDER, S_REGISTERFIBTH);

--COUNTER
COUNTER_INCREMENT: INCREMENT 				generic map(BUS_WIDTH => BUS_WIDTH)
													port map (S_REGISTER_COUNT, S_INCREMENT_COUNT);
													
COUNTER_MUX: MUX2X1 					generic map(BUS_WIDTH => BUS_WIDTH)
													port map (x"0001", S_INCREMENT_COUNT, sel_count, S_MUX_COUNT);
													
COUNTER_REGISTER: NBIT_REGISTER 			generic map(BUS_WIDTH => BUS_WIDTH)
													port map(clk, reset, en_count, S_MUX_COUNT, S_REGISTER_COUNT);
													
COUNTER_COMPARATOR: COMPARATOR2X1 		generic map(BUS_WIDTH => BUS_WIDTH)
													port map(S_REGISTER_COUNT, N, comp_res);

--COMPARADORES
N_EQUAL_ONE_COMPARATOR: COMPARATOR2X1 	generic map(BUS_WIDTH => BUS_WIDTH)
													port map(N, x"0001", n_eq_one);

N_EQUAL_ZERO_COMPARATOR: COMPARATOR2X1 	generic map(BUS_WIDTH => BUS_WIDTH)
													port map(N, x"0000", n_eq_zero);

--MULTIPLEXERS DE AÇÃO DOS COMPARADORES
ACTION_MUX1: MUX2X1 					generic map(BUS_WIDTH => BUS_WIDTH)
											port map(x"0001", x"0000", sel_cond1, S_ACTIONMUX1);

ACTION_MUX2: MUX2X1 					generic map(BUS_WIDTH => BUS_WIDTH)
											port map(S_REGISTERFIBTH, S_ACTIONMUX1, sel_cond2, R);

--TESTE DO CONTADOR
--COUNTER_OUT <= S_REGISTER_COUNT; 						

--TESTES DE PHI
--F1 <= S_REGISTERF1;
--F2 <= S_REGISTERF2;
--R <= S_REGISTERFIBTH;


end architecture;
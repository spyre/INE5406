library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity INCREMENT is
generic(BUS_WIDTH: POSITIVE := 32);
port(A: in std_logic_vector(BUS_WIDTH-1 downto 0);
	  B: out std_logic_vector(BUS_WIDTH-1 downto 0)
);

end entity;

architecture bhv of INCREMENT is
signal temp: unsigned(BUS_WIDTH-1 downto 0);

begin
temp <= to_unsigned(1, temp'length);

B <= std_logic_vector(unsigned(A) + temp);

end architecture;
library IEEE;
use IEEE.std_logic_1164.all;

entity MUX2X1 is
generic(BUS_WIDTH: POSITIVE := 32);
port(
	A: in std_logic_vector(BUS_WIDTH-1 downto 0);
	B: in std_logic_vector(BUS_WIDTH-1 downto 0);
	SEL: in std_logic;
	C: out std_logic_vector(BUS_WIDTH-1 downto 0)
);

end entity;

architecture bhv of MUX2X1 is
begin

	with SEL select
		C <= A when	'0',
			  B when others;

end architecture;

library ieee;
use ieee.std_logic_1164.all;

entity AVALON_WRAPPER is
port(
	--READS FROM COMPONENT
	readdata: out std_logic_vector(31 downto 0);
	read: in std_logic; -- not used. just declared to comply with the standard

	--WRITES FROM COMPONENTS
	write: in std_logic;
	writedata: in std_logic_vector(31 downto 0); 

	clock, reset: in std_logic
);

end entity;

architecture WRAPPING of AVALON_WRAPPER is

component NBIT_REGISTER is
generic(BUS_WIDTH: POSITIVE := 32);
	port(
		-- Input ports
		clk, reset, en: in std_logic;	

		-- Output ports
		d: in std_logic_vector(BUS_WIDTH-1 downto 0);
		q: out std_logic_vector(BUS_WIDTH-1 downto 0)
	);
end component;

component NABLA is
generic(BUS_WIDTH: POSITIVE := 16);
port(
	N: in std_logic_vector(BUS_WIDTH-1 downto 0);
	R: out std_logic_vector(BUS_WIDTH-1 downto 0);
	
	clk, rst, iniciar: in std_logic;
	
	ready: out std_logic
	
	--phi: out std_logic_vector(31 downto 0)
);
end component;

signal N_SIGNAL_INPUT: std_logic_vector(15 downto 0); --DO FF DA ENTRADA PARA "N" DO SISTEMA
signal R_TO_REGISTER: std_logic_vector(15 downto 0); --DO "R" DO SISTEMA PARA O FF DO WRAPPER

signal EN_OUTPUT_CASEREADY: std_logic;
signal R_REGISTER_OUTPUT: std_logic_vector(15 downto 0);
signal READY_OUTPUT: std_logic;

begin

INPUT_REGISTER: NBIT_REGISTER
			generic map (BUS_WIDTH => 16)
			port map (clock, reset, write, writedata(15 downto 1)&'0', N_SIGNAL_INPUT); --first 16 bits of writedata go into the N input
			
OUTPUT_REGISTER: NBIT_REGISTER
			generic map (BUS_WIDTH => 16)
			port map (clock, reset, READY_OUTPUT, R_TO_REGISTER, R_REGISTER_OUTPUT); --register output has two internal signals

WRAPPED: NABLA 
			generic map(BUS_WIDTH => 16)
			port map(N_SIGNAL_INPUT, R_TO_REGISTER, clock, reset, write AND writedata(0), READY_OUTPUT);

readdata <= R_REGISTER_OUTPUT & READY_OUTPUT & x"000" & "000";
			
end architecture;
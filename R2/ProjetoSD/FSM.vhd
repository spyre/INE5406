library ieee;
use ieee.std_logic_1164.all;

entity FSM is
	port(
		-- control inputs
		clk, rst: in std_logic;
		iniciar, n_eq_one, n_eq_zero, comp_res: in std_logic;
		-- control outputs
		sel_f2, en_f1, en_f2, en_fibth, sel_cond1, sel_cond2, sel_count, en_count, ready, reset_bo: out std_logic
		--en_phi: out std_logic;
		);
end entity; 

architecture controle of FSM  is
	type InternalState is (Q0, Q1, Q2, Q3, Q4, Q5, Q6, Q7, Q8, Q9);
	signal nextState, currentState: InternalState;
begin
	NSL: process(currentState, iniciar, n_eq_one, n_eq_zero) is
	begin
		nextState <= currentState;
		case currentState is
			when Q0 =>
				if iniciar= '1' then 
					nextState <= Q1;
				elsif iniciar='0' then
					nextState <= Q0;
				end if;
			
			when Q1 =>
				if n_eq_one= '1' then 
					nextState <= Q3;
				elsif n_eq_zero = '1' then
					nextState <= Q2;
				else
					nextState <= Q4;
				end if;
				
			when Q2 => 
					nextState <= Q0;
				
			when Q3 =>
					nextState <= Q0;
					
			when Q4 =>
					nextState <= Q5;
			
			when Q5 =>
					nextState <= Q6;
					
			when Q6 =>
				nextState <= Q7;

			when Q7 => 
				if comp_res = '0' then
					nextState <= Q5;
				elsif comp_res = '1' then
					nextState <= Q8;
				end if;

			when Q8 =>
				nextState <= Q9;
				
			when Q9 =>
				nextState <= Q9; --nextState <= Q0 se preferir resetar tudo

		end case;
	end process;

	-- memory element (sequential)
	process(clk, rst) is
	begin
		if rst='1' then
			currentState <= q0; -- reset state
		elsif rising_edge(clk) then 
			currentState <= nextState;
		end if;
	end process;
	
	-- output-logic 
			
	sel_f2 <= 
			'1' when currentState = Q7 else
			'0';
			
	en_f1 <= 
			'1' when currentState = Q6 else
			'0';
			
	en_f2 <= 
			'1' when currentState = Q4 else
			'1' when currentState = Q7 else
			'0';
	
	en_fibth <= 
			'1' when currentState = Q5 else
			'0';
			
	sel_cond1 <= 
			'1' when currentState = Q2 else
		    '0';
		
	sel_cond2 <= 
			'1' when currentState = Q2 else
			'1' when currentState = Q3 else
			'0';
	
	--en_phi <= 
	--		'1' when currentState = Q8 else
	--		'0';
	
	sel_count <= 
			'1' when currentState = Q7 else
			'0';

	en_count <= 
			'1' when currentState = Q4 else
			'1' when currentState = Q7 else 
			'0';
			
	ready <= 
			'1' when currentState = Q9 else
			'0';

	reset_bo <=
			'1' when currentState = Q0 else
			'0';
			
end architecture;
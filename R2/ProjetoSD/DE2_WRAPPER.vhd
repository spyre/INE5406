library ieee;
use ieee.std_logic_1164.all;

entity DE2_WRAPPER is 
port(

--LEDR: out std_logic_vector(17 downto 0);
LEDG: out std_logic_vector(1 downto 0);
CLOCK_50: in std_logic;
SW: in std_logic_vector(17 downto 0); --SW(17) usado pra INICIAR, SW(16) usado pra RESET
HEX0: out std_logic_vector(6 downto 0);
HEX1: out std_logic_vector(6 downto 0);
HEX2: out std_logic_vector(6 downto 0);
HEX3: out std_logic_vector(6 downto 0);
HEX4: out std_logic_vector(6 downto 0);
HEX5: out std_logic_vector(6 downto 0);
HEX6: out std_logic_vector(6 downto 0);
HEX7: out std_logic_vector(6 downto 0)

);

end entity;

architecture BHV of DE2_WRAPPER is
component NABLA is
generic(BUS_WIDTH: POSITIVE := 16);
port(
	N: in std_logic_vector(BUS_WIDTH-1 downto 0);
	R: out std_logic_vector(BUS_WIDTH-1 downto 0);
	
	clk, rst, iniciar: in std_logic;
	
	ready: out std_logic
	
	--phi: out std_logic_vector(31 downto 0)
);
end component;

component bcd is
      port (
        in_binary :  in std_logic_vector(31 downto 0);
        digit_0   : out std_logic_vector( 3 downto 0);
        digit_1   : out std_logic_vector( 3 downto 0);
        digit_2   : out std_logic_vector( 3 downto 0);
        digit_3   : out std_logic_vector( 3 downto 0);
        digit_4   : out std_logic_vector( 3 downto 0);
        digit_5   : out std_logic_vector( 3 downto 0);
        digit_6   : out std_logic_vector( 3 downto 0);
        digit_7   : out std_logic_vector( 3 downto 0);
        digit_8   : out std_logic_vector( 3 downto 0);
        digit_9   : out std_logic_vector( 3 downto 0)
      );
end component;

component DECODER is
port 
( C: in std_logic_vector(3 downto 0);
  F: out std_logic_vector(6 downto 0)
);
end component;

signal binary_nabla_output: std_logic_vector(15 downto 0);
signal todecode0, todecode1, todecode2, todecode3, todecode4, todecode5, todecode6, todecode7: std_logic_vector(3 downto 0);
begin

SD: NABLA generic map(BUS_WIDTH => 16)
			 port map(SW(15 downto 0), binary_nabla_output(15 downto 0), CLOCK_50, SW(16), SW(17), LEDG(0));
<<<<<<< HEAD

BCD_CONV: bcd port map(x"0000" & binary_nabla_output(15 downto 0),todecode0, todecode1, todecode2, todecode3, todecode4, todecode5, todecode6, todecode7);

TO7SEG0: DECODER port map(todecode0, HEX0);
TO7SEG1: DECODER port map(todecode1, HEX1);
TO7SEG2: DECODER port map(todecode2, HEX2);
TO7SEG3: DECODER port map(todecode3, HEX3);
TO7SEG4: DECODER port map(todecode4, HEX4);
TO7SEG5: DECODER port map(todecode5, HEX5);
TO7SEG6: DECODER port map(todecode6, HEX6);
TO7SEG7: DECODER port map(todecode7, HEX7);

=======
>>>>>>> upstream/master

BCD_CONV: bcd port map(x"0000" & binary_nabla_output(15 downto 0),todecode0, todecode1, todecode2, todecode3, todecode4, todecode5, todecode6, todecode7);

TO7SEG0: DECODER port map(todecode0, HEX0);
TO7SEG1: DECODER port map(todecode1, HEX1);
TO7SEG2: DECODER port map(todecode2, HEX2);
TO7SEG3: DECODER port map(todecode3, HEX3);
TO7SEG4: DECODER port map(todecode4, HEX4);
TO7SEG5: DECODER port map(todecode5, HEX5);
TO7SEG6: DECODER port map(todecode6, HEX6);
TO7SEG7: DECODER port map(todecode7, HEX7);


end architecture;

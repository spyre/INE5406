-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 32-bit"
-- VERSION "Version 13.0.1 Build 232 06/12/2013 Service Pack 1 SJ Web Edition"

-- DATE "11/16/2018 15:44:38"

-- 
-- Device: Altera EP2C35F672C6 Package FBGA672
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEII;
LIBRARY IEEE;
USE CYCLONEII.CYCLONEII_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	NABLA IS
    PORT (
	N : IN std_logic_vector(15 DOWNTO 0);
	R : OUT std_logic_vector(15 DOWNTO 0);
	clk : IN std_logic;
	rst : IN std_logic;
	iniciar : IN std_logic;
	ready : OUT std_logic
	);
END NABLA;

-- Design Ports Information
-- R[0]	=>  Location: PIN_C16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- R[1]	=>  Location: PIN_G13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- R[2]	=>  Location: PIN_B14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- R[3]	=>  Location: PIN_C12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- R[4]	=>  Location: PIN_G14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- R[5]	=>  Location: PIN_B12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- R[6]	=>  Location: PIN_F13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- R[7]	=>  Location: PIN_A14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- R[8]	=>  Location: PIN_C15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- R[9]	=>  Location: PIN_D16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- R[10]	=>  Location: PIN_C11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- R[11]	=>  Location: PIN_B16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- R[12]	=>  Location: PIN_B15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- R[13]	=>  Location: PIN_F14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- R[14]	=>  Location: PIN_D15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- R[15]	=>  Location: PIN_D14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- ready	=>  Location: PIN_C10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- N[0]	=>  Location: PIN_C13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- N[1]	=>  Location: PIN_D13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- N[2]	=>  Location: PIN_D12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- N[3]	=>  Location: PIN_D11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- N[4]	=>  Location: PIN_F12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- N[5]	=>  Location: PIN_G12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- N[6]	=>  Location: PIN_B9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- N[7]	=>  Location: PIN_A10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- N[8]	=>  Location: PIN_B10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- N[9]	=>  Location: PIN_D10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- N[10]	=>  Location: PIN_J11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- N[11]	=>  Location: PIN_E12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- N[12]	=>  Location: PIN_G11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- N[13]	=>  Location: PIN_J10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- N[14]	=>  Location: PIN_J14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- N[15]	=>  Location: PIN_J13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- clk	=>  Location: PIN_P2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- rst	=>  Location: PIN_P1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- iniciar	=>  Location: PIN_B11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default


ARCHITECTURE structure OF NABLA IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_N : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_R : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_clk : std_logic;
SIGNAL ww_rst : std_logic;
SIGNAL ww_iniciar : std_logic;
SIGNAL ww_ready : std_logic;
SIGNAL \clk~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \rst~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \DATAPATH|COUNTER_REGISTER|currentState[3]~22_combout\ : std_logic;
SIGNAL \DATAPATH|COUNTER_REGISTER|currentState[12]~40_combout\ : std_logic;
SIGNAL \DATAPATH|N_EQUAL_ZERO_COMPARATOR|Equal0~0_combout\ : std_logic;
SIGNAL \DATAPATH|COUNTER_COMPARATOR|Equal0~0_combout\ : std_logic;
SIGNAL \DATAPATH|COUNTER_COMPARATOR|Equal0~7_combout\ : std_logic;
SIGNAL \DATAPATH|N_EQUAL_ZERO_COMPARATOR|Equal0~3_combout\ : std_logic;
SIGNAL \DATAPATH|N_EQUAL_ZERO_COMPARATOR|Equal0~2_combout\ : std_logic;
SIGNAL \DATAPATH|N_EQUAL_ZERO_COMPARATOR|Equal0~1_combout\ : std_logic;
SIGNAL \DATAPATH|N_EQUAL_ZERO_COMPARATOR|Equal0~4_combout\ : std_logic;
SIGNAL \CONTROL|Selector3~0_combout\ : std_logic;
SIGNAL \rst~combout\ : std_logic;
SIGNAL \rst~clkctrl_outclk\ : std_logic;
SIGNAL \CONTROL|currentState.Q3~regout\ : std_logic;
SIGNAL \CONTROL|Selector2~0_combout\ : std_logic;
SIGNAL \CONTROL|currentState.Q2~regout\ : std_logic;
SIGNAL \iniciar~combout\ : std_logic;
SIGNAL \CONTROL|Selector0~0_combout\ : std_logic;
SIGNAL \CONTROL|currentState.Q0~regout\ : std_logic;
SIGNAL \CONTROL|Selector1~0_combout\ : std_logic;
SIGNAL \CONTROL|currentState.Q1~regout\ : std_logic;
SIGNAL \CONTROL|Selector4~0_combout\ : std_logic;
SIGNAL \CONTROL|currentState.Q4~regout\ : std_logic;
SIGNAL \DATAPATH|COUNTER_REGISTER|currentState[0]~16_combout\ : std_logic;
SIGNAL \CONTROL|en_count~0_combout\ : std_logic;
SIGNAL \DATAPATH|COUNTER_REGISTER|currentState[0]~17\ : std_logic;
SIGNAL \DATAPATH|COUNTER_REGISTER|currentState[1]~18_combout\ : std_logic;
SIGNAL \~GND~combout\ : std_logic;
SIGNAL \DATAPATH|COUNTER_REGISTER|currentState[1]~19\ : std_logic;
SIGNAL \DATAPATH|COUNTER_REGISTER|currentState[2]~20_combout\ : std_logic;
SIGNAL \DATAPATH|COUNTER_REGISTER|currentState[2]~21\ : std_logic;
SIGNAL \DATAPATH|COUNTER_REGISTER|currentState[3]~23\ : std_logic;
SIGNAL \DATAPATH|COUNTER_REGISTER|currentState[4]~24_combout\ : std_logic;
SIGNAL \DATAPATH|COUNTER_REGISTER|currentState[4]~25\ : std_logic;
SIGNAL \DATAPATH|COUNTER_REGISTER|currentState[5]~27\ : std_logic;
SIGNAL \DATAPATH|COUNTER_REGISTER|currentState[6]~29\ : std_logic;
SIGNAL \DATAPATH|COUNTER_REGISTER|currentState[7]~30_combout\ : std_logic;
SIGNAL \DATAPATH|COUNTER_REGISTER|currentState[7]~31\ : std_logic;
SIGNAL \DATAPATH|COUNTER_REGISTER|currentState[8]~33\ : std_logic;
SIGNAL \DATAPATH|COUNTER_REGISTER|currentState[9]~34_combout\ : std_logic;
SIGNAL \DATAPATH|COUNTER_REGISTER|currentState[9]~35\ : std_logic;
SIGNAL \DATAPATH|COUNTER_REGISTER|currentState[10]~36_combout\ : std_logic;
SIGNAL \DATAPATH|COUNTER_REGISTER|currentState[10]~37\ : std_logic;
SIGNAL \DATAPATH|COUNTER_REGISTER|currentState[11]~38_combout\ : std_logic;
SIGNAL \DATAPATH|COUNTER_COMPARATOR|Equal0~6_combout\ : std_logic;
SIGNAL \DATAPATH|COUNTER_REGISTER|currentState[8]~32_combout\ : std_logic;
SIGNAL \DATAPATH|COUNTER_COMPARATOR|Equal0~5_combout\ : std_logic;
SIGNAL \DATAPATH|COUNTER_REGISTER|currentState[11]~39\ : std_logic;
SIGNAL \DATAPATH|COUNTER_REGISTER|currentState[12]~41\ : std_logic;
SIGNAL \DATAPATH|COUNTER_REGISTER|currentState[13]~42_combout\ : std_logic;
SIGNAL \DATAPATH|COUNTER_REGISTER|currentState[13]~43\ : std_logic;
SIGNAL \DATAPATH|COUNTER_REGISTER|currentState[14]~44_combout\ : std_logic;
SIGNAL \DATAPATH|COUNTER_REGISTER|currentState[14]~45\ : std_logic;
SIGNAL \DATAPATH|COUNTER_REGISTER|currentState[15]~46_combout\ : std_logic;
SIGNAL \DATAPATH|COUNTER_COMPARATOR|Equal0~8_combout\ : std_logic;
SIGNAL \DATAPATH|COUNTER_COMPARATOR|Equal0~9_combout\ : std_logic;
SIGNAL \DATAPATH|COUNTER_REGISTER|currentState[5]~26_combout\ : std_logic;
SIGNAL \DATAPATH|COUNTER_COMPARATOR|Equal0~2_combout\ : std_logic;
SIGNAL \DATAPATH|COUNTER_REGISTER|currentState[6]~28_combout\ : std_logic;
SIGNAL \DATAPATH|COUNTER_COMPARATOR|Equal0~3_combout\ : std_logic;
SIGNAL \DATAPATH|COUNTER_COMPARATOR|Equal0~1_combout\ : std_logic;
SIGNAL \DATAPATH|COUNTER_COMPARATOR|Equal0~4_combout\ : std_logic;
SIGNAL \CONTROL|Selector5~0_combout\ : std_logic;
SIGNAL \CONTROL|currentState.Q5~regout\ : std_logic;
SIGNAL \CONTROL|currentState.Q6~regout\ : std_logic;
SIGNAL \CONTROL|currentState.Q7~regout\ : std_logic;
SIGNAL \DATAPATH|MUXF2|C[0]~0_combout\ : std_logic;
SIGNAL \DATAPATH|REGISTERFITBH|currentState[0]~16_combout\ : std_logic;
SIGNAL \DATAPATH|ACTION_MUX2|C[0]~0_combout\ : std_logic;
SIGNAL \DATAPATH|MUXF2|C[1]~1_combout\ : std_logic;
SIGNAL \DATAPATH|REGISTERFITBH|currentState[0]~17\ : std_logic;
SIGNAL \DATAPATH|REGISTERFITBH|currentState[1]~18_combout\ : std_logic;
SIGNAL \DATAPATH|ACTION_MUX2|C[1]~1_combout\ : std_logic;
SIGNAL \DATAPATH|MUXF2|C[2]~2_combout\ : std_logic;
SIGNAL \DATAPATH|REGISTERFITBH|currentState[1]~19\ : std_logic;
SIGNAL \DATAPATH|REGISTERFITBH|currentState[2]~20_combout\ : std_logic;
SIGNAL \DATAPATH|ACTION_MUX2|C[2]~2_combout\ : std_logic;
SIGNAL \DATAPATH|MUXF2|C[3]~3_combout\ : std_logic;
SIGNAL \DATAPATH|REGISTERFITBH|currentState[2]~21\ : std_logic;
SIGNAL \DATAPATH|REGISTERFITBH|currentState[3]~22_combout\ : std_logic;
SIGNAL \DATAPATH|ACTION_MUX2|C[3]~3_combout\ : std_logic;
SIGNAL \DATAPATH|MUXF2|C[4]~4_combout\ : std_logic;
SIGNAL \DATAPATH|REGISTERFITBH|currentState[3]~23\ : std_logic;
SIGNAL \DATAPATH|REGISTERFITBH|currentState[4]~24_combout\ : std_logic;
SIGNAL \DATAPATH|ACTION_MUX2|C[4]~4_combout\ : std_logic;
SIGNAL \DATAPATH|MUXF2|C[5]~5_combout\ : std_logic;
SIGNAL \DATAPATH|REGISTERFITBH|currentState[4]~25\ : std_logic;
SIGNAL \DATAPATH|REGISTERFITBH|currentState[5]~26_combout\ : std_logic;
SIGNAL \DATAPATH|ACTION_MUX2|C[5]~5_combout\ : std_logic;
SIGNAL \DATAPATH|MUXF2|C[6]~6_combout\ : std_logic;
SIGNAL \DATAPATH|REGISTERFITBH|currentState[5]~27\ : std_logic;
SIGNAL \DATAPATH|REGISTERFITBH|currentState[6]~28_combout\ : std_logic;
SIGNAL \DATAPATH|ACTION_MUX2|C[6]~6_combout\ : std_logic;
SIGNAL \DATAPATH|MUXF2|C[7]~7_combout\ : std_logic;
SIGNAL \DATAPATH|REGISTERFITBH|currentState[6]~29\ : std_logic;
SIGNAL \DATAPATH|REGISTERFITBH|currentState[7]~30_combout\ : std_logic;
SIGNAL \DATAPATH|ACTION_MUX2|C[7]~7_combout\ : std_logic;
SIGNAL \DATAPATH|MUXF2|C[8]~8_combout\ : std_logic;
SIGNAL \DATAPATH|REGISTERFITBH|currentState[7]~31\ : std_logic;
SIGNAL \DATAPATH|REGISTERFITBH|currentState[8]~32_combout\ : std_logic;
SIGNAL \DATAPATH|ACTION_MUX2|C[8]~8_combout\ : std_logic;
SIGNAL \DATAPATH|MUXF2|C[9]~9_combout\ : std_logic;
SIGNAL \DATAPATH|REGISTERFITBH|currentState[8]~33\ : std_logic;
SIGNAL \DATAPATH|REGISTERFITBH|currentState[9]~34_combout\ : std_logic;
SIGNAL \DATAPATH|ACTION_MUX2|C[9]~9_combout\ : std_logic;
SIGNAL \DATAPATH|MUXF2|C[10]~10_combout\ : std_logic;
SIGNAL \DATAPATH|REGISTERFITBH|currentState[9]~35\ : std_logic;
SIGNAL \DATAPATH|REGISTERFITBH|currentState[10]~36_combout\ : std_logic;
SIGNAL \DATAPATH|ACTION_MUX2|C[10]~10_combout\ : std_logic;
SIGNAL \DATAPATH|MUXF2|C[11]~11_combout\ : std_logic;
SIGNAL \DATAPATH|REGISTERFITBH|currentState[10]~37\ : std_logic;
SIGNAL \DATAPATH|REGISTERFITBH|currentState[11]~38_combout\ : std_logic;
SIGNAL \DATAPATH|ACTION_MUX2|C[11]~11_combout\ : std_logic;
SIGNAL \DATAPATH|MUXF2|C[12]~12_combout\ : std_logic;
SIGNAL \DATAPATH|REGISTERFITBH|currentState[11]~39\ : std_logic;
SIGNAL \DATAPATH|REGISTERFITBH|currentState[12]~40_combout\ : std_logic;
SIGNAL \DATAPATH|ACTION_MUX2|C[12]~12_combout\ : std_logic;
SIGNAL \DATAPATH|MUXF2|C[13]~13_combout\ : std_logic;
SIGNAL \DATAPATH|REGISTERFITBH|currentState[12]~41\ : std_logic;
SIGNAL \DATAPATH|REGISTERFITBH|currentState[13]~42_combout\ : std_logic;
SIGNAL \DATAPATH|ACTION_MUX2|C[13]~13_combout\ : std_logic;
SIGNAL \DATAPATH|MUXF2|C[14]~14_combout\ : std_logic;
SIGNAL \DATAPATH|REGISTERFITBH|currentState[13]~43\ : std_logic;
SIGNAL \DATAPATH|REGISTERFITBH|currentState[14]~44_combout\ : std_logic;
SIGNAL \DATAPATH|ACTION_MUX2|C[14]~14_combout\ : std_logic;
SIGNAL \DATAPATH|MUXF2|C[15]~15_combout\ : std_logic;
SIGNAL \DATAPATH|REGISTERFITBH|currentState[14]~45\ : std_logic;
SIGNAL \DATAPATH|REGISTERFITBH|currentState[15]~46_combout\ : std_logic;
SIGNAL \DATAPATH|ACTION_MUX2|C[15]~15_combout\ : std_logic;
SIGNAL \clk~combout\ : std_logic;
SIGNAL \clk~clkctrl_outclk\ : std_logic;
SIGNAL \CONTROL|Selector8~0_combout\ : std_logic;
SIGNAL \CONTROL|currentState.Q8~regout\ : std_logic;
SIGNAL \CONTROL|currentState.Q9~0_combout\ : std_logic;
SIGNAL \CONTROL|currentState.Q9~regout\ : std_logic;
SIGNAL \DATAPATH|REGISTERF1|currentState\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \DATAPATH|REGISTERF2|currentState\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \DATAPATH|REGISTERFITBH|currentState\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \DATAPATH|COUNTER_REGISTER|currentState\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \N~combout\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CONTROL|ALT_INV_currentState.Q7~regout\ : std_logic;
SIGNAL \CONTROL|ALT_INV_currentState.Q0~regout\ : std_logic;

BEGIN

ww_N <= N;
R <= ww_R;
ww_clk <= clk;
ww_rst <= rst;
ww_iniciar <= iniciar;
ready <= ww_ready;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\clk~clkctrl_INCLK_bus\ <= (gnd & gnd & gnd & \clk~combout\);

\rst~clkctrl_INCLK_bus\ <= (gnd & gnd & gnd & \rst~combout\);
\CONTROL|ALT_INV_currentState.Q7~regout\ <= NOT \CONTROL|currentState.Q7~regout\;
\CONTROL|ALT_INV_currentState.Q0~regout\ <= NOT \CONTROL|currentState.Q0~regout\;

-- Location: LCFF_X28_Y34_N7
\DATAPATH|COUNTER_REGISTER|currentState[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|COUNTER_REGISTER|currentState[3]~22_combout\,
	sdata => \~GND~combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	sload => \CONTROL|ALT_INV_currentState.Q7~regout\,
	ena => \CONTROL|en_count~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|COUNTER_REGISTER|currentState\(3));

-- Location: LCFF_X28_Y34_N25
\DATAPATH|COUNTER_REGISTER|currentState[12]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|COUNTER_REGISTER|currentState[12]~40_combout\,
	sdata => \~GND~combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	sload => \CONTROL|ALT_INV_currentState.Q7~regout\,
	ena => \CONTROL|en_count~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|COUNTER_REGISTER|currentState\(12));

-- Location: LCCOMB_X28_Y34_N6
\DATAPATH|COUNTER_REGISTER|currentState[3]~22\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|COUNTER_REGISTER|currentState[3]~22_combout\ = (\DATAPATH|COUNTER_REGISTER|currentState\(3) & (!\DATAPATH|COUNTER_REGISTER|currentState[2]~21\)) # (!\DATAPATH|COUNTER_REGISTER|currentState\(3) & ((\DATAPATH|COUNTER_REGISTER|currentState[2]~21\) 
-- # (GND)))
-- \DATAPATH|COUNTER_REGISTER|currentState[3]~23\ = CARRY((!\DATAPATH|COUNTER_REGISTER|currentState[2]~21\) # (!\DATAPATH|COUNTER_REGISTER|currentState\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \DATAPATH|COUNTER_REGISTER|currentState\(3),
	datad => VCC,
	cin => \DATAPATH|COUNTER_REGISTER|currentState[2]~21\,
	combout => \DATAPATH|COUNTER_REGISTER|currentState[3]~22_combout\,
	cout => \DATAPATH|COUNTER_REGISTER|currentState[3]~23\);

-- Location: LCCOMB_X28_Y34_N24
\DATAPATH|COUNTER_REGISTER|currentState[12]~40\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|COUNTER_REGISTER|currentState[12]~40_combout\ = (\DATAPATH|COUNTER_REGISTER|currentState\(12) & (\DATAPATH|COUNTER_REGISTER|currentState[11]~39\ $ (GND))) # (!\DATAPATH|COUNTER_REGISTER|currentState\(12) & 
-- (!\DATAPATH|COUNTER_REGISTER|currentState[11]~39\ & VCC))
-- \DATAPATH|COUNTER_REGISTER|currentState[12]~41\ = CARRY((\DATAPATH|COUNTER_REGISTER|currentState\(12) & !\DATAPATH|COUNTER_REGISTER|currentState[11]~39\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \DATAPATH|COUNTER_REGISTER|currentState\(12),
	datad => VCC,
	cin => \DATAPATH|COUNTER_REGISTER|currentState[11]~39\,
	combout => \DATAPATH|COUNTER_REGISTER|currentState[12]~40_combout\,
	cout => \DATAPATH|COUNTER_REGISTER|currentState[12]~41\);

-- Location: LCCOMB_X27_Y34_N12
\DATAPATH|N_EQUAL_ZERO_COMPARATOR|Equal0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|N_EQUAL_ZERO_COMPARATOR|Equal0~0_combout\ = (!\N~combout\(3) & (!\N~combout\(2) & (!\N~combout\(4) & !\N~combout\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \N~combout\(3),
	datab => \N~combout\(2),
	datac => \N~combout\(4),
	datad => \N~combout\(1),
	combout => \DATAPATH|N_EQUAL_ZERO_COMPARATOR|Equal0~0_combout\);

-- Location: LCFF_X30_Y34_N15
\DATAPATH|REGISTERF1|currentState[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	sdata => \DATAPATH|REGISTERF2|currentState\(1),
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	sload => VCC,
	ena => \CONTROL|currentState.Q6~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERF1|currentState\(1));

-- Location: LCFF_X32_Y34_N3
\DATAPATH|REGISTERF1|currentState[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	sdata => \DATAPATH|REGISTERF2|currentState\(2),
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	sload => VCC,
	ena => \CONTROL|currentState.Q6~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERF1|currentState\(2));

-- Location: LCFF_X30_Y34_N13
\DATAPATH|REGISTERF1|currentState[8]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	sdata => \DATAPATH|REGISTERF2|currentState\(8),
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	sload => VCC,
	ena => \CONTROL|currentState.Q6~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERF1|currentState\(8));

-- Location: LCFF_X30_Y34_N11
\DATAPATH|REGISTERF1|currentState[10]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	sdata => \DATAPATH|REGISTERF2|currentState\(10),
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	sload => VCC,
	ena => \CONTROL|currentState.Q6~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERF1|currentState\(10));

-- Location: LCFF_X32_Y34_N17
\DATAPATH|REGISTERF1|currentState[11]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	sdata => \DATAPATH|REGISTERF2|currentState\(11),
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	sload => VCC,
	ena => \CONTROL|currentState.Q6~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERF1|currentState\(11));

-- Location: LCFF_X30_Y34_N9
\DATAPATH|REGISTERF1|currentState[12]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	sdata => \DATAPATH|REGISTERF2|currentState\(12),
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	sload => VCC,
	ena => \CONTROL|currentState.Q6~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERF1|currentState\(12));

-- Location: LCCOMB_X29_Y34_N14
\DATAPATH|COUNTER_COMPARATOR|Equal0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|COUNTER_COMPARATOR|Equal0~0_combout\ = (\N~combout\(1) & (\DATAPATH|COUNTER_REGISTER|currentState\(1) & (\N~combout\(0) $ (!\DATAPATH|COUNTER_REGISTER|currentState\(0))))) # (!\N~combout\(1) & (!\DATAPATH|COUNTER_REGISTER|currentState\(1) & 
-- (\N~combout\(0) $ (!\DATAPATH|COUNTER_REGISTER|currentState\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000001001000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \N~combout\(1),
	datab => \N~combout\(0),
	datac => \DATAPATH|COUNTER_REGISTER|currentState\(0),
	datad => \DATAPATH|COUNTER_REGISTER|currentState\(1),
	combout => \DATAPATH|COUNTER_COMPARATOR|Equal0~0_combout\);

-- Location: LCCOMB_X27_Y34_N28
\DATAPATH|COUNTER_COMPARATOR|Equal0~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|COUNTER_COMPARATOR|Equal0~7_combout\ = (\N~combout\(12) & (\DATAPATH|COUNTER_REGISTER|currentState\(12) & (\N~combout\(13) $ (!\DATAPATH|COUNTER_REGISTER|currentState\(13))))) # (!\N~combout\(12) & (!\DATAPATH|COUNTER_REGISTER|currentState\(12) 
-- & (\N~combout\(13) $ (!\DATAPATH|COUNTER_REGISTER|currentState\(13)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000001001000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \N~combout\(12),
	datab => \N~combout\(13),
	datac => \DATAPATH|COUNTER_REGISTER|currentState\(13),
	datad => \DATAPATH|COUNTER_REGISTER|currentState\(12),
	combout => \DATAPATH|COUNTER_COMPARATOR|Equal0~7_combout\);

-- Location: PIN_D13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\N[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_N(1),
	combout => \N~combout\(1));

-- Location: PIN_G11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\N[12]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_N(12),
	combout => \N~combout\(12));

-- Location: PIN_J13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\N[15]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_N(15),
	combout => \N~combout\(15));

-- Location: PIN_J10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\N[13]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_N(13),
	combout => \N~combout\(13));

-- Location: LCCOMB_X27_Y34_N2
\DATAPATH|N_EQUAL_ZERO_COMPARATOR|Equal0~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|N_EQUAL_ZERO_COMPARATOR|Equal0~3_combout\ = (!\N~combout\(14) & (!\N~combout\(15) & !\N~combout\(13)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \N~combout\(14),
	datab => \N~combout\(15),
	datac => \N~combout\(13),
	combout => \DATAPATH|N_EQUAL_ZERO_COMPARATOR|Equal0~3_combout\);

-- Location: PIN_D10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\N[9]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_N(9),
	combout => \N~combout\(9));

-- Location: PIN_J11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\N[10]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_N(10),
	combout => \N~combout\(10));

-- Location: PIN_E12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\N[11]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_N(11),
	combout => \N~combout\(11));

-- Location: LCCOMB_X27_Y34_N20
\DATAPATH|N_EQUAL_ZERO_COMPARATOR|Equal0~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|N_EQUAL_ZERO_COMPARATOR|Equal0~2_combout\ = (!\N~combout\(12) & (!\N~combout\(9) & (!\N~combout\(10) & !\N~combout\(11))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \N~combout\(12),
	datab => \N~combout\(9),
	datac => \N~combout\(10),
	datad => \N~combout\(11),
	combout => \DATAPATH|N_EQUAL_ZERO_COMPARATOR|Equal0~2_combout\);

-- Location: PIN_B10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\N[8]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_N(8),
	combout => \N~combout\(8));

-- Location: PIN_B9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\N[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_N(6),
	combout => \N~combout\(6));

-- Location: PIN_G12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\N[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_N(5),
	combout => \N~combout\(5));

-- Location: LCCOMB_X27_Y34_N18
\DATAPATH|N_EQUAL_ZERO_COMPARATOR|Equal0~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|N_EQUAL_ZERO_COMPARATOR|Equal0~1_combout\ = (!\N~combout\(7) & (!\N~combout\(8) & (!\N~combout\(6) & !\N~combout\(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \N~combout\(7),
	datab => \N~combout\(8),
	datac => \N~combout\(6),
	datad => \N~combout\(5),
	combout => \DATAPATH|N_EQUAL_ZERO_COMPARATOR|Equal0~1_combout\);

-- Location: LCCOMB_X27_Y34_N24
\DATAPATH|N_EQUAL_ZERO_COMPARATOR|Equal0~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|N_EQUAL_ZERO_COMPARATOR|Equal0~4_combout\ = (\DATAPATH|N_EQUAL_ZERO_COMPARATOR|Equal0~0_combout\ & (\DATAPATH|N_EQUAL_ZERO_COMPARATOR|Equal0~3_combout\ & (\DATAPATH|N_EQUAL_ZERO_COMPARATOR|Equal0~2_combout\ & 
-- \DATAPATH|N_EQUAL_ZERO_COMPARATOR|Equal0~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DATAPATH|N_EQUAL_ZERO_COMPARATOR|Equal0~0_combout\,
	datab => \DATAPATH|N_EQUAL_ZERO_COMPARATOR|Equal0~3_combout\,
	datac => \DATAPATH|N_EQUAL_ZERO_COMPARATOR|Equal0~2_combout\,
	datad => \DATAPATH|N_EQUAL_ZERO_COMPARATOR|Equal0~1_combout\,
	combout => \DATAPATH|N_EQUAL_ZERO_COMPARATOR|Equal0~4_combout\);

-- Location: PIN_C13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\N[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_N(0),
	combout => \N~combout\(0));

-- Location: LCCOMB_X29_Y34_N4
\CONTROL|Selector3~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \CONTROL|Selector3~0_combout\ = (\CONTROL|currentState.Q1~regout\ & (\DATAPATH|N_EQUAL_ZERO_COMPARATOR|Equal0~4_combout\ & \N~combout\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \CONTROL|currentState.Q1~regout\,
	datac => \DATAPATH|N_EQUAL_ZERO_COMPARATOR|Equal0~4_combout\,
	datad => \N~combout\(0),
	combout => \CONTROL|Selector3~0_combout\);

-- Location: PIN_P1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\rst~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_rst,
	combout => \rst~combout\);

-- Location: CLKCTRL_G1
\rst~clkctrl\ : cycloneii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \rst~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \rst~clkctrl_outclk\);

-- Location: LCFF_X29_Y34_N5
\CONTROL|currentState.Q3\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \CONTROL|Selector3~0_combout\,
	aclr => \rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \CONTROL|currentState.Q3~regout\);

-- Location: LCCOMB_X29_Y34_N18
\CONTROL|Selector2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \CONTROL|Selector2~0_combout\ = (\CONTROL|currentState.Q1~regout\ & (\DATAPATH|N_EQUAL_ZERO_COMPARATOR|Equal0~4_combout\ & !\N~combout\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \CONTROL|currentState.Q1~regout\,
	datac => \DATAPATH|N_EQUAL_ZERO_COMPARATOR|Equal0~4_combout\,
	datad => \N~combout\(0),
	combout => \CONTROL|Selector2~0_combout\);

-- Location: LCFF_X29_Y34_N19
\CONTROL|currentState.Q2\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \CONTROL|Selector2~0_combout\,
	aclr => \rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \CONTROL|currentState.Q2~regout\);

-- Location: PIN_B11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\iniciar~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_iniciar,
	combout => \iniciar~combout\);

-- Location: LCCOMB_X29_Y34_N22
\CONTROL|Selector0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \CONTROL|Selector0~0_combout\ = (!\CONTROL|currentState.Q3~regout\ & (!\CONTROL|currentState.Q2~regout\ & ((\iniciar~combout\) # (\CONTROL|currentState.Q0~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \iniciar~combout\,
	datab => \CONTROL|currentState.Q3~regout\,
	datac => \CONTROL|currentState.Q0~regout\,
	datad => \CONTROL|currentState.Q2~regout\,
	combout => \CONTROL|Selector0~0_combout\);

-- Location: LCFF_X29_Y34_N23
\CONTROL|currentState.Q0\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \CONTROL|Selector0~0_combout\,
	aclr => \rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \CONTROL|currentState.Q0~regout\);

-- Location: LCCOMB_X29_Y34_N12
\CONTROL|Selector1~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \CONTROL|Selector1~0_combout\ = (\iniciar~combout\ & !\CONTROL|currentState.Q0~regout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \iniciar~combout\,
	datad => \CONTROL|currentState.Q0~regout\,
	combout => \CONTROL|Selector1~0_combout\);

-- Location: LCFF_X29_Y34_N13
\CONTROL|currentState.Q1\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \CONTROL|Selector1~0_combout\,
	aclr => \rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \CONTROL|currentState.Q1~regout\);

-- Location: LCCOMB_X29_Y34_N6
\CONTROL|Selector4~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \CONTROL|Selector4~0_combout\ = (!\DATAPATH|N_EQUAL_ZERO_COMPARATOR|Equal0~4_combout\ & \CONTROL|currentState.Q1~regout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \DATAPATH|N_EQUAL_ZERO_COMPARATOR|Equal0~4_combout\,
	datad => \CONTROL|currentState.Q1~regout\,
	combout => \CONTROL|Selector4~0_combout\);

-- Location: LCFF_X29_Y34_N7
\CONTROL|currentState.Q4\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \CONTROL|Selector4~0_combout\,
	aclr => \rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \CONTROL|currentState.Q4~regout\);

-- Location: LCCOMB_X28_Y34_N0
\DATAPATH|COUNTER_REGISTER|currentState[0]~16\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|COUNTER_REGISTER|currentState[0]~16_combout\ = \DATAPATH|COUNTER_REGISTER|currentState\(0) $ (VCC)
-- \DATAPATH|COUNTER_REGISTER|currentState[0]~17\ = CARRY(\DATAPATH|COUNTER_REGISTER|currentState\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DATAPATH|COUNTER_REGISTER|currentState\(0),
	datad => VCC,
	combout => \DATAPATH|COUNTER_REGISTER|currentState[0]~16_combout\,
	cout => \DATAPATH|COUNTER_REGISTER|currentState[0]~17\);

-- Location: LCCOMB_X29_Y34_N30
\CONTROL|en_count~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \CONTROL|en_count~0_combout\ = \CONTROL|currentState.Q7~regout\ $ (\CONTROL|currentState.Q4~regout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \CONTROL|currentState.Q7~regout\,
	datad => \CONTROL|currentState.Q4~regout\,
	combout => \CONTROL|en_count~0_combout\);

-- Location: LCFF_X28_Y34_N1
\DATAPATH|COUNTER_REGISTER|currentState[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|COUNTER_REGISTER|currentState[0]~16_combout\,
	sdata => VCC,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	sload => \CONTROL|ALT_INV_currentState.Q7~regout\,
	ena => \CONTROL|en_count~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|COUNTER_REGISTER|currentState\(0));

-- Location: LCCOMB_X28_Y34_N2
\DATAPATH|COUNTER_REGISTER|currentState[1]~18\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|COUNTER_REGISTER|currentState[1]~18_combout\ = (\DATAPATH|COUNTER_REGISTER|currentState\(1) & (!\DATAPATH|COUNTER_REGISTER|currentState[0]~17\)) # (!\DATAPATH|COUNTER_REGISTER|currentState\(1) & ((\DATAPATH|COUNTER_REGISTER|currentState[0]~17\) 
-- # (GND)))
-- \DATAPATH|COUNTER_REGISTER|currentState[1]~19\ = CARRY((!\DATAPATH|COUNTER_REGISTER|currentState[0]~17\) # (!\DATAPATH|COUNTER_REGISTER|currentState\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \DATAPATH|COUNTER_REGISTER|currentState\(1),
	datad => VCC,
	cin => \DATAPATH|COUNTER_REGISTER|currentState[0]~17\,
	combout => \DATAPATH|COUNTER_REGISTER|currentState[1]~18_combout\,
	cout => \DATAPATH|COUNTER_REGISTER|currentState[1]~19\);

-- Location: LCCOMB_X27_Y34_N0
\~GND\ : cycloneii_lcell_comb
-- Equation(s):
-- \~GND~combout\ = GND

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	combout => \~GND~combout\);

-- Location: LCFF_X28_Y34_N3
\DATAPATH|COUNTER_REGISTER|currentState[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|COUNTER_REGISTER|currentState[1]~18_combout\,
	sdata => \~GND~combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	sload => \CONTROL|ALT_INV_currentState.Q7~regout\,
	ena => \CONTROL|en_count~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|COUNTER_REGISTER|currentState\(1));

-- Location: LCCOMB_X28_Y34_N4
\DATAPATH|COUNTER_REGISTER|currentState[2]~20\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|COUNTER_REGISTER|currentState[2]~20_combout\ = (\DATAPATH|COUNTER_REGISTER|currentState\(2) & (\DATAPATH|COUNTER_REGISTER|currentState[1]~19\ $ (GND))) # (!\DATAPATH|COUNTER_REGISTER|currentState\(2) & 
-- (!\DATAPATH|COUNTER_REGISTER|currentState[1]~19\ & VCC))
-- \DATAPATH|COUNTER_REGISTER|currentState[2]~21\ = CARRY((\DATAPATH|COUNTER_REGISTER|currentState\(2) & !\DATAPATH|COUNTER_REGISTER|currentState[1]~19\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \DATAPATH|COUNTER_REGISTER|currentState\(2),
	datad => VCC,
	cin => \DATAPATH|COUNTER_REGISTER|currentState[1]~19\,
	combout => \DATAPATH|COUNTER_REGISTER|currentState[2]~20_combout\,
	cout => \DATAPATH|COUNTER_REGISTER|currentState[2]~21\);

-- Location: LCFF_X28_Y34_N5
\DATAPATH|COUNTER_REGISTER|currentState[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|COUNTER_REGISTER|currentState[2]~20_combout\,
	sdata => \~GND~combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	sload => \CONTROL|ALT_INV_currentState.Q7~regout\,
	ena => \CONTROL|en_count~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|COUNTER_REGISTER|currentState\(2));

-- Location: LCCOMB_X28_Y34_N8
\DATAPATH|COUNTER_REGISTER|currentState[4]~24\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|COUNTER_REGISTER|currentState[4]~24_combout\ = (\DATAPATH|COUNTER_REGISTER|currentState\(4) & (\DATAPATH|COUNTER_REGISTER|currentState[3]~23\ $ (GND))) # (!\DATAPATH|COUNTER_REGISTER|currentState\(4) & 
-- (!\DATAPATH|COUNTER_REGISTER|currentState[3]~23\ & VCC))
-- \DATAPATH|COUNTER_REGISTER|currentState[4]~25\ = CARRY((\DATAPATH|COUNTER_REGISTER|currentState\(4) & !\DATAPATH|COUNTER_REGISTER|currentState[3]~23\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \DATAPATH|COUNTER_REGISTER|currentState\(4),
	datad => VCC,
	cin => \DATAPATH|COUNTER_REGISTER|currentState[3]~23\,
	combout => \DATAPATH|COUNTER_REGISTER|currentState[4]~24_combout\,
	cout => \DATAPATH|COUNTER_REGISTER|currentState[4]~25\);

-- Location: LCFF_X28_Y34_N9
\DATAPATH|COUNTER_REGISTER|currentState[4]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|COUNTER_REGISTER|currentState[4]~24_combout\,
	sdata => \~GND~combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	sload => \CONTROL|ALT_INV_currentState.Q7~regout\,
	ena => \CONTROL|en_count~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|COUNTER_REGISTER|currentState\(4));

-- Location: LCCOMB_X28_Y34_N10
\DATAPATH|COUNTER_REGISTER|currentState[5]~26\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|COUNTER_REGISTER|currentState[5]~26_combout\ = (\DATAPATH|COUNTER_REGISTER|currentState\(5) & (!\DATAPATH|COUNTER_REGISTER|currentState[4]~25\)) # (!\DATAPATH|COUNTER_REGISTER|currentState\(5) & ((\DATAPATH|COUNTER_REGISTER|currentState[4]~25\) 
-- # (GND)))
-- \DATAPATH|COUNTER_REGISTER|currentState[5]~27\ = CARRY((!\DATAPATH|COUNTER_REGISTER|currentState[4]~25\) # (!\DATAPATH|COUNTER_REGISTER|currentState\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \DATAPATH|COUNTER_REGISTER|currentState\(5),
	datad => VCC,
	cin => \DATAPATH|COUNTER_REGISTER|currentState[4]~25\,
	combout => \DATAPATH|COUNTER_REGISTER|currentState[5]~26_combout\,
	cout => \DATAPATH|COUNTER_REGISTER|currentState[5]~27\);

-- Location: LCCOMB_X28_Y34_N12
\DATAPATH|COUNTER_REGISTER|currentState[6]~28\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|COUNTER_REGISTER|currentState[6]~28_combout\ = (\DATAPATH|COUNTER_REGISTER|currentState\(6) & (\DATAPATH|COUNTER_REGISTER|currentState[5]~27\ $ (GND))) # (!\DATAPATH|COUNTER_REGISTER|currentState\(6) & 
-- (!\DATAPATH|COUNTER_REGISTER|currentState[5]~27\ & VCC))
-- \DATAPATH|COUNTER_REGISTER|currentState[6]~29\ = CARRY((\DATAPATH|COUNTER_REGISTER|currentState\(6) & !\DATAPATH|COUNTER_REGISTER|currentState[5]~27\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \DATAPATH|COUNTER_REGISTER|currentState\(6),
	datad => VCC,
	cin => \DATAPATH|COUNTER_REGISTER|currentState[5]~27\,
	combout => \DATAPATH|COUNTER_REGISTER|currentState[6]~28_combout\,
	cout => \DATAPATH|COUNTER_REGISTER|currentState[6]~29\);

-- Location: LCCOMB_X28_Y34_N14
\DATAPATH|COUNTER_REGISTER|currentState[7]~30\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|COUNTER_REGISTER|currentState[7]~30_combout\ = (\DATAPATH|COUNTER_REGISTER|currentState\(7) & (!\DATAPATH|COUNTER_REGISTER|currentState[6]~29\)) # (!\DATAPATH|COUNTER_REGISTER|currentState\(7) & ((\DATAPATH|COUNTER_REGISTER|currentState[6]~29\) 
-- # (GND)))
-- \DATAPATH|COUNTER_REGISTER|currentState[7]~31\ = CARRY((!\DATAPATH|COUNTER_REGISTER|currentState[6]~29\) # (!\DATAPATH|COUNTER_REGISTER|currentState\(7)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \DATAPATH|COUNTER_REGISTER|currentState\(7),
	datad => VCC,
	cin => \DATAPATH|COUNTER_REGISTER|currentState[6]~29\,
	combout => \DATAPATH|COUNTER_REGISTER|currentState[7]~30_combout\,
	cout => \DATAPATH|COUNTER_REGISTER|currentState[7]~31\);

-- Location: LCFF_X28_Y34_N15
\DATAPATH|COUNTER_REGISTER|currentState[7]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|COUNTER_REGISTER|currentState[7]~30_combout\,
	sdata => \~GND~combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	sload => \CONTROL|ALT_INV_currentState.Q7~regout\,
	ena => \CONTROL|en_count~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|COUNTER_REGISTER|currentState\(7));

-- Location: LCCOMB_X28_Y34_N16
\DATAPATH|COUNTER_REGISTER|currentState[8]~32\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|COUNTER_REGISTER|currentState[8]~32_combout\ = (\DATAPATH|COUNTER_REGISTER|currentState\(8) & (\DATAPATH|COUNTER_REGISTER|currentState[7]~31\ $ (GND))) # (!\DATAPATH|COUNTER_REGISTER|currentState\(8) & 
-- (!\DATAPATH|COUNTER_REGISTER|currentState[7]~31\ & VCC))
-- \DATAPATH|COUNTER_REGISTER|currentState[8]~33\ = CARRY((\DATAPATH|COUNTER_REGISTER|currentState\(8) & !\DATAPATH|COUNTER_REGISTER|currentState[7]~31\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \DATAPATH|COUNTER_REGISTER|currentState\(8),
	datad => VCC,
	cin => \DATAPATH|COUNTER_REGISTER|currentState[7]~31\,
	combout => \DATAPATH|COUNTER_REGISTER|currentState[8]~32_combout\,
	cout => \DATAPATH|COUNTER_REGISTER|currentState[8]~33\);

-- Location: LCCOMB_X28_Y34_N18
\DATAPATH|COUNTER_REGISTER|currentState[9]~34\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|COUNTER_REGISTER|currentState[9]~34_combout\ = (\DATAPATH|COUNTER_REGISTER|currentState\(9) & (!\DATAPATH|COUNTER_REGISTER|currentState[8]~33\)) # (!\DATAPATH|COUNTER_REGISTER|currentState\(9) & ((\DATAPATH|COUNTER_REGISTER|currentState[8]~33\) 
-- # (GND)))
-- \DATAPATH|COUNTER_REGISTER|currentState[9]~35\ = CARRY((!\DATAPATH|COUNTER_REGISTER|currentState[8]~33\) # (!\DATAPATH|COUNTER_REGISTER|currentState\(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \DATAPATH|COUNTER_REGISTER|currentState\(9),
	datad => VCC,
	cin => \DATAPATH|COUNTER_REGISTER|currentState[8]~33\,
	combout => \DATAPATH|COUNTER_REGISTER|currentState[9]~34_combout\,
	cout => \DATAPATH|COUNTER_REGISTER|currentState[9]~35\);

-- Location: LCFF_X28_Y34_N19
\DATAPATH|COUNTER_REGISTER|currentState[9]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|COUNTER_REGISTER|currentState[9]~34_combout\,
	sdata => \~GND~combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	sload => \CONTROL|ALT_INV_currentState.Q7~regout\,
	ena => \CONTROL|en_count~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|COUNTER_REGISTER|currentState\(9));

-- Location: LCCOMB_X28_Y34_N20
\DATAPATH|COUNTER_REGISTER|currentState[10]~36\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|COUNTER_REGISTER|currentState[10]~36_combout\ = (\DATAPATH|COUNTER_REGISTER|currentState\(10) & (\DATAPATH|COUNTER_REGISTER|currentState[9]~35\ $ (GND))) # (!\DATAPATH|COUNTER_REGISTER|currentState\(10) & 
-- (!\DATAPATH|COUNTER_REGISTER|currentState[9]~35\ & VCC))
-- \DATAPATH|COUNTER_REGISTER|currentState[10]~37\ = CARRY((\DATAPATH|COUNTER_REGISTER|currentState\(10) & !\DATAPATH|COUNTER_REGISTER|currentState[9]~35\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \DATAPATH|COUNTER_REGISTER|currentState\(10),
	datad => VCC,
	cin => \DATAPATH|COUNTER_REGISTER|currentState[9]~35\,
	combout => \DATAPATH|COUNTER_REGISTER|currentState[10]~36_combout\,
	cout => \DATAPATH|COUNTER_REGISTER|currentState[10]~37\);

-- Location: LCFF_X28_Y34_N21
\DATAPATH|COUNTER_REGISTER|currentState[10]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|COUNTER_REGISTER|currentState[10]~36_combout\,
	sdata => \~GND~combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	sload => \CONTROL|ALT_INV_currentState.Q7~regout\,
	ena => \CONTROL|en_count~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|COUNTER_REGISTER|currentState\(10));

-- Location: LCCOMB_X28_Y34_N22
\DATAPATH|COUNTER_REGISTER|currentState[11]~38\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|COUNTER_REGISTER|currentState[11]~38_combout\ = (\DATAPATH|COUNTER_REGISTER|currentState\(11) & (!\DATAPATH|COUNTER_REGISTER|currentState[10]~37\)) # (!\DATAPATH|COUNTER_REGISTER|currentState\(11) & 
-- ((\DATAPATH|COUNTER_REGISTER|currentState[10]~37\) # (GND)))
-- \DATAPATH|COUNTER_REGISTER|currentState[11]~39\ = CARRY((!\DATAPATH|COUNTER_REGISTER|currentState[10]~37\) # (!\DATAPATH|COUNTER_REGISTER|currentState\(11)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \DATAPATH|COUNTER_REGISTER|currentState\(11),
	datad => VCC,
	cin => \DATAPATH|COUNTER_REGISTER|currentState[10]~37\,
	combout => \DATAPATH|COUNTER_REGISTER|currentState[11]~38_combout\,
	cout => \DATAPATH|COUNTER_REGISTER|currentState[11]~39\);

-- Location: LCFF_X28_Y34_N23
\DATAPATH|COUNTER_REGISTER|currentState[11]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|COUNTER_REGISTER|currentState[11]~38_combout\,
	sdata => \~GND~combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	sload => \CONTROL|ALT_INV_currentState.Q7~regout\,
	ena => \CONTROL|en_count~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|COUNTER_REGISTER|currentState\(11));

-- Location: LCCOMB_X27_Y34_N26
\DATAPATH|COUNTER_COMPARATOR|Equal0~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|COUNTER_COMPARATOR|Equal0~6_combout\ = (\N~combout\(11) & (\DATAPATH|COUNTER_REGISTER|currentState\(11) & (\DATAPATH|COUNTER_REGISTER|currentState\(10) $ (!\N~combout\(10))))) # (!\N~combout\(11) & (!\DATAPATH|COUNTER_REGISTER|currentState\(11) 
-- & (\DATAPATH|COUNTER_REGISTER|currentState\(10) $ (!\N~combout\(10)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000001001000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \N~combout\(11),
	datab => \DATAPATH|COUNTER_REGISTER|currentState\(10),
	datac => \N~combout\(10),
	datad => \DATAPATH|COUNTER_REGISTER|currentState\(11),
	combout => \DATAPATH|COUNTER_COMPARATOR|Equal0~6_combout\);

-- Location: LCFF_X28_Y34_N17
\DATAPATH|COUNTER_REGISTER|currentState[8]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|COUNTER_REGISTER|currentState[8]~32_combout\,
	sdata => \~GND~combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	sload => \CONTROL|ALT_INV_currentState.Q7~regout\,
	ena => \CONTROL|en_count~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|COUNTER_REGISTER|currentState\(8));

-- Location: LCCOMB_X27_Y34_N16
\DATAPATH|COUNTER_COMPARATOR|Equal0~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|COUNTER_COMPARATOR|Equal0~5_combout\ = (\N~combout\(9) & (\DATAPATH|COUNTER_REGISTER|currentState\(9) & (\N~combout\(8) $ (!\DATAPATH|COUNTER_REGISTER|currentState\(8))))) # (!\N~combout\(9) & (!\DATAPATH|COUNTER_REGISTER|currentState\(9) & 
-- (\N~combout\(8) $ (!\DATAPATH|COUNTER_REGISTER|currentState\(8)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000001001000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \N~combout\(9),
	datab => \N~combout\(8),
	datac => \DATAPATH|COUNTER_REGISTER|currentState\(8),
	datad => \DATAPATH|COUNTER_REGISTER|currentState\(9),
	combout => \DATAPATH|COUNTER_COMPARATOR|Equal0~5_combout\);

-- Location: LCCOMB_X28_Y34_N26
\DATAPATH|COUNTER_REGISTER|currentState[13]~42\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|COUNTER_REGISTER|currentState[13]~42_combout\ = (\DATAPATH|COUNTER_REGISTER|currentState\(13) & (!\DATAPATH|COUNTER_REGISTER|currentState[12]~41\)) # (!\DATAPATH|COUNTER_REGISTER|currentState\(13) & 
-- ((\DATAPATH|COUNTER_REGISTER|currentState[12]~41\) # (GND)))
-- \DATAPATH|COUNTER_REGISTER|currentState[13]~43\ = CARRY((!\DATAPATH|COUNTER_REGISTER|currentState[12]~41\) # (!\DATAPATH|COUNTER_REGISTER|currentState\(13)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \DATAPATH|COUNTER_REGISTER|currentState\(13),
	datad => VCC,
	cin => \DATAPATH|COUNTER_REGISTER|currentState[12]~41\,
	combout => \DATAPATH|COUNTER_REGISTER|currentState[13]~42_combout\,
	cout => \DATAPATH|COUNTER_REGISTER|currentState[13]~43\);

-- Location: LCFF_X28_Y34_N27
\DATAPATH|COUNTER_REGISTER|currentState[13]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|COUNTER_REGISTER|currentState[13]~42_combout\,
	sdata => \~GND~combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	sload => \CONTROL|ALT_INV_currentState.Q7~regout\,
	ena => \CONTROL|en_count~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|COUNTER_REGISTER|currentState\(13));

-- Location: LCCOMB_X28_Y34_N28
\DATAPATH|COUNTER_REGISTER|currentState[14]~44\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|COUNTER_REGISTER|currentState[14]~44_combout\ = (\DATAPATH|COUNTER_REGISTER|currentState\(14) & (\DATAPATH|COUNTER_REGISTER|currentState[13]~43\ $ (GND))) # (!\DATAPATH|COUNTER_REGISTER|currentState\(14) & 
-- (!\DATAPATH|COUNTER_REGISTER|currentState[13]~43\ & VCC))
-- \DATAPATH|COUNTER_REGISTER|currentState[14]~45\ = CARRY((\DATAPATH|COUNTER_REGISTER|currentState\(14) & !\DATAPATH|COUNTER_REGISTER|currentState[13]~43\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \DATAPATH|COUNTER_REGISTER|currentState\(14),
	datad => VCC,
	cin => \DATAPATH|COUNTER_REGISTER|currentState[13]~43\,
	combout => \DATAPATH|COUNTER_REGISTER|currentState[14]~44_combout\,
	cout => \DATAPATH|COUNTER_REGISTER|currentState[14]~45\);

-- Location: LCFF_X28_Y34_N29
\DATAPATH|COUNTER_REGISTER|currentState[14]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|COUNTER_REGISTER|currentState[14]~44_combout\,
	sdata => \~GND~combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	sload => \CONTROL|ALT_INV_currentState.Q7~regout\,
	ena => \CONTROL|en_count~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|COUNTER_REGISTER|currentState\(14));

-- Location: PIN_J14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\N[14]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_N(14),
	combout => \N~combout\(14));

-- Location: LCCOMB_X28_Y34_N30
\DATAPATH|COUNTER_REGISTER|currentState[15]~46\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|COUNTER_REGISTER|currentState[15]~46_combout\ = \DATAPATH|COUNTER_REGISTER|currentState[14]~45\ $ (\DATAPATH|COUNTER_REGISTER|currentState\(15))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \DATAPATH|COUNTER_REGISTER|currentState\(15),
	cin => \DATAPATH|COUNTER_REGISTER|currentState[14]~45\,
	combout => \DATAPATH|COUNTER_REGISTER|currentState[15]~46_combout\);

-- Location: LCFF_X28_Y34_N31
\DATAPATH|COUNTER_REGISTER|currentState[15]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|COUNTER_REGISTER|currentState[15]~46_combout\,
	sdata => \~GND~combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	sload => \CONTROL|ALT_INV_currentState.Q7~regout\,
	ena => \CONTROL|en_count~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|COUNTER_REGISTER|currentState\(15));

-- Location: LCCOMB_X27_Y34_N10
\DATAPATH|COUNTER_COMPARATOR|Equal0~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|COUNTER_COMPARATOR|Equal0~8_combout\ = (\N~combout\(15) & (\DATAPATH|COUNTER_REGISTER|currentState\(15) & (\DATAPATH|COUNTER_REGISTER|currentState\(14) $ (!\N~combout\(14))))) # (!\N~combout\(15) & (!\DATAPATH|COUNTER_REGISTER|currentState\(15) 
-- & (\DATAPATH|COUNTER_REGISTER|currentState\(14) $ (!\N~combout\(14)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000001001000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \N~combout\(15),
	datab => \DATAPATH|COUNTER_REGISTER|currentState\(14),
	datac => \N~combout\(14),
	datad => \DATAPATH|COUNTER_REGISTER|currentState\(15),
	combout => \DATAPATH|COUNTER_COMPARATOR|Equal0~8_combout\);

-- Location: LCCOMB_X25_Y34_N4
\DATAPATH|COUNTER_COMPARATOR|Equal0~9\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|COUNTER_COMPARATOR|Equal0~9_combout\ = (\DATAPATH|COUNTER_COMPARATOR|Equal0~7_combout\ & (\DATAPATH|COUNTER_COMPARATOR|Equal0~6_combout\ & (\DATAPATH|COUNTER_COMPARATOR|Equal0~5_combout\ & \DATAPATH|COUNTER_COMPARATOR|Equal0~8_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DATAPATH|COUNTER_COMPARATOR|Equal0~7_combout\,
	datab => \DATAPATH|COUNTER_COMPARATOR|Equal0~6_combout\,
	datac => \DATAPATH|COUNTER_COMPARATOR|Equal0~5_combout\,
	datad => \DATAPATH|COUNTER_COMPARATOR|Equal0~8_combout\,
	combout => \DATAPATH|COUNTER_COMPARATOR|Equal0~9_combout\);

-- Location: PIN_F12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\N[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_N(4),
	combout => \N~combout\(4));

-- Location: LCFF_X28_Y34_N11
\DATAPATH|COUNTER_REGISTER|currentState[5]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|COUNTER_REGISTER|currentState[5]~26_combout\,
	sdata => \~GND~combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	sload => \CONTROL|ALT_INV_currentState.Q7~regout\,
	ena => \CONTROL|en_count~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|COUNTER_REGISTER|currentState\(5));

-- Location: LCCOMB_X27_Y34_N8
\DATAPATH|COUNTER_COMPARATOR|Equal0~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|COUNTER_COMPARATOR|Equal0~2_combout\ = (\N~combout\(5) & (\DATAPATH|COUNTER_REGISTER|currentState\(5) & (\N~combout\(4) $ (!\DATAPATH|COUNTER_REGISTER|currentState\(4))))) # (!\N~combout\(5) & (!\DATAPATH|COUNTER_REGISTER|currentState\(5) & 
-- (\N~combout\(4) $ (!\DATAPATH|COUNTER_REGISTER|currentState\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000001001000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \N~combout\(5),
	datab => \N~combout\(4),
	datac => \DATAPATH|COUNTER_REGISTER|currentState\(4),
	datad => \DATAPATH|COUNTER_REGISTER|currentState\(5),
	combout => \DATAPATH|COUNTER_COMPARATOR|Equal0~2_combout\);

-- Location: LCFF_X28_Y34_N13
\DATAPATH|COUNTER_REGISTER|currentState[6]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|COUNTER_REGISTER|currentState[6]~28_combout\,
	sdata => \~GND~combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	sload => \CONTROL|ALT_INV_currentState.Q7~regout\,
	ena => \CONTROL|en_count~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|COUNTER_REGISTER|currentState\(6));

-- Location: PIN_A10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\N[7]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_N(7),
	combout => \N~combout\(7));

-- Location: LCCOMB_X27_Y34_N6
\DATAPATH|COUNTER_COMPARATOR|Equal0~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|COUNTER_COMPARATOR|Equal0~3_combout\ = (\N~combout\(6) & (\DATAPATH|COUNTER_REGISTER|currentState\(6) & (\DATAPATH|COUNTER_REGISTER|currentState\(7) $ (!\N~combout\(7))))) # (!\N~combout\(6) & (!\DATAPATH|COUNTER_REGISTER|currentState\(6) & 
-- (\DATAPATH|COUNTER_REGISTER|currentState\(7) $ (!\N~combout\(7)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001000000001001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \N~combout\(6),
	datab => \DATAPATH|COUNTER_REGISTER|currentState\(6),
	datac => \DATAPATH|COUNTER_REGISTER|currentState\(7),
	datad => \N~combout\(7),
	combout => \DATAPATH|COUNTER_COMPARATOR|Equal0~3_combout\);

-- Location: PIN_D12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\N[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_N(2),
	combout => \N~combout\(2));

-- Location: PIN_D11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\N[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_N(3),
	combout => \N~combout\(3));

-- Location: LCCOMB_X27_Y34_N22
\DATAPATH|COUNTER_COMPARATOR|Equal0~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|COUNTER_COMPARATOR|Equal0~1_combout\ = (\DATAPATH|COUNTER_REGISTER|currentState\(3) & (\N~combout\(3) & (\N~combout\(2) $ (!\DATAPATH|COUNTER_REGISTER|currentState\(2))))) # (!\DATAPATH|COUNTER_REGISTER|currentState\(3) & (!\N~combout\(3) & 
-- (\N~combout\(2) $ (!\DATAPATH|COUNTER_REGISTER|currentState\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010000100001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DATAPATH|COUNTER_REGISTER|currentState\(3),
	datab => \N~combout\(2),
	datac => \N~combout\(3),
	datad => \DATAPATH|COUNTER_REGISTER|currentState\(2),
	combout => \DATAPATH|COUNTER_COMPARATOR|Equal0~1_combout\);

-- Location: LCCOMB_X25_Y34_N30
\DATAPATH|COUNTER_COMPARATOR|Equal0~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|COUNTER_COMPARATOR|Equal0~4_combout\ = (\DATAPATH|COUNTER_COMPARATOR|Equal0~0_combout\ & (\DATAPATH|COUNTER_COMPARATOR|Equal0~2_combout\ & (\DATAPATH|COUNTER_COMPARATOR|Equal0~3_combout\ & \DATAPATH|COUNTER_COMPARATOR|Equal0~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DATAPATH|COUNTER_COMPARATOR|Equal0~0_combout\,
	datab => \DATAPATH|COUNTER_COMPARATOR|Equal0~2_combout\,
	datac => \DATAPATH|COUNTER_COMPARATOR|Equal0~3_combout\,
	datad => \DATAPATH|COUNTER_COMPARATOR|Equal0~1_combout\,
	combout => \DATAPATH|COUNTER_COMPARATOR|Equal0~4_combout\);

-- Location: LCCOMB_X25_Y34_N2
\CONTROL|Selector5~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \CONTROL|Selector5~0_combout\ = (\CONTROL|currentState.Q4~regout\) # ((\CONTROL|currentState.Q7~regout\ & ((!\DATAPATH|COUNTER_COMPARATOR|Equal0~4_combout\) # (!\DATAPATH|COUNTER_COMPARATOR|Equal0~9_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111011101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \CONTROL|currentState.Q7~regout\,
	datab => \CONTROL|currentState.Q4~regout\,
	datac => \DATAPATH|COUNTER_COMPARATOR|Equal0~9_combout\,
	datad => \DATAPATH|COUNTER_COMPARATOR|Equal0~4_combout\,
	combout => \CONTROL|Selector5~0_combout\);

-- Location: LCFF_X25_Y34_N3
\CONTROL|currentState.Q5\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \CONTROL|Selector5~0_combout\,
	aclr => \rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \CONTROL|currentState.Q5~regout\);

-- Location: LCFF_X29_Y34_N9
\CONTROL|currentState.Q6\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	sdata => \CONTROL|currentState.Q5~regout\,
	aclr => \rst~clkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \CONTROL|currentState.Q6~regout\);

-- Location: LCFF_X29_Y34_N17
\CONTROL|currentState.Q7\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	sdata => \CONTROL|currentState.Q6~regout\,
	aclr => \rst~clkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \CONTROL|currentState.Q7~regout\);

-- Location: LCCOMB_X30_Y34_N30
\DATAPATH|MUXF2|C[0]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|MUXF2|C[0]~0_combout\ = (\DATAPATH|REGISTERFITBH|currentState\(0)) # (!\CONTROL|currentState.Q7~regout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \DATAPATH|REGISTERFITBH|currentState\(0),
	datad => \CONTROL|currentState.Q7~regout\,
	combout => \DATAPATH|MUXF2|C[0]~0_combout\);

-- Location: LCFF_X30_Y34_N31
\DATAPATH|REGISTERF2|currentState[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|MUXF2|C[0]~0_combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	ena => \CONTROL|en_count~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERF2|currentState\(0));

-- Location: LCFF_X30_Y34_N17
\DATAPATH|REGISTERF1|currentState[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	sdata => \DATAPATH|REGISTERF2|currentState\(0),
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	sload => VCC,
	ena => \CONTROL|currentState.Q6~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERF1|currentState\(0));

-- Location: LCCOMB_X31_Y34_N0
\DATAPATH|REGISTERFITBH|currentState[0]~16\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|REGISTERFITBH|currentState[0]~16_combout\ = (\DATAPATH|REGISTERF2|currentState\(0) & (\DATAPATH|REGISTERF1|currentState\(0) $ (VCC))) # (!\DATAPATH|REGISTERF2|currentState\(0) & (\DATAPATH|REGISTERF1|currentState\(0) & VCC))
-- \DATAPATH|REGISTERFITBH|currentState[0]~17\ = CARRY((\DATAPATH|REGISTERF2|currentState\(0) & \DATAPATH|REGISTERF1|currentState\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110011010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DATAPATH|REGISTERF2|currentState\(0),
	datab => \DATAPATH|REGISTERF1|currentState\(0),
	datad => VCC,
	combout => \DATAPATH|REGISTERFITBH|currentState[0]~16_combout\,
	cout => \DATAPATH|REGISTERFITBH|currentState[0]~17\);

-- Location: LCFF_X31_Y34_N1
\DATAPATH|REGISTERFITBH|currentState[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|REGISTERFITBH|currentState[0]~16_combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	ena => \CONTROL|currentState.Q5~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERFITBH|currentState\(0));

-- Location: LCCOMB_X30_Y34_N22
\DATAPATH|ACTION_MUX2|C[0]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|ACTION_MUX2|C[0]~0_combout\ = (\CONTROL|currentState.Q3~regout\ & ((\DATAPATH|REGISTERFITBH|currentState\(0)) # (!\CONTROL|currentState.Q2~regout\))) # (!\CONTROL|currentState.Q3~regout\ & (!\CONTROL|currentState.Q2~regout\ & 
-- \DATAPATH|REGISTERFITBH|currentState\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101100100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \CONTROL|currentState.Q3~regout\,
	datab => \CONTROL|currentState.Q2~regout\,
	datad => \DATAPATH|REGISTERFITBH|currentState\(0),
	combout => \DATAPATH|ACTION_MUX2|C[0]~0_combout\);

-- Location: LCCOMB_X32_Y34_N18
\DATAPATH|MUXF2|C[1]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|MUXF2|C[1]~1_combout\ = (\DATAPATH|REGISTERFITBH|currentState\(1) & \CONTROL|currentState.Q7~regout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DATAPATH|REGISTERFITBH|currentState\(1),
	datad => \CONTROL|currentState.Q7~regout\,
	combout => \DATAPATH|MUXF2|C[1]~1_combout\);

-- Location: LCFF_X32_Y34_N19
\DATAPATH|REGISTERF2|currentState[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|MUXF2|C[1]~1_combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	ena => \CONTROL|en_count~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERF2|currentState\(1));

-- Location: LCCOMB_X31_Y34_N2
\DATAPATH|REGISTERFITBH|currentState[1]~18\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|REGISTERFITBH|currentState[1]~18_combout\ = (\DATAPATH|REGISTERF1|currentState\(1) & ((\DATAPATH|REGISTERF2|currentState\(1) & (\DATAPATH|REGISTERFITBH|currentState[0]~17\ & VCC)) # (!\DATAPATH|REGISTERF2|currentState\(1) & 
-- (!\DATAPATH|REGISTERFITBH|currentState[0]~17\)))) # (!\DATAPATH|REGISTERF1|currentState\(1) & ((\DATAPATH|REGISTERF2|currentState\(1) & (!\DATAPATH|REGISTERFITBH|currentState[0]~17\)) # (!\DATAPATH|REGISTERF2|currentState\(1) & 
-- ((\DATAPATH|REGISTERFITBH|currentState[0]~17\) # (GND)))))
-- \DATAPATH|REGISTERFITBH|currentState[1]~19\ = CARRY((\DATAPATH|REGISTERF1|currentState\(1) & (!\DATAPATH|REGISTERF2|currentState\(1) & !\DATAPATH|REGISTERFITBH|currentState[0]~17\)) # (!\DATAPATH|REGISTERF1|currentState\(1) & 
-- ((!\DATAPATH|REGISTERFITBH|currentState[0]~17\) # (!\DATAPATH|REGISTERF2|currentState\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \DATAPATH|REGISTERF1|currentState\(1),
	datab => \DATAPATH|REGISTERF2|currentState\(1),
	datad => VCC,
	cin => \DATAPATH|REGISTERFITBH|currentState[0]~17\,
	combout => \DATAPATH|REGISTERFITBH|currentState[1]~18_combout\,
	cout => \DATAPATH|REGISTERFITBH|currentState[1]~19\);

-- Location: LCFF_X31_Y34_N3
\DATAPATH|REGISTERFITBH|currentState[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|REGISTERFITBH|currentState[1]~18_combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	ena => \CONTROL|currentState.Q5~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERFITBH|currentState\(1));

-- Location: LCCOMB_X32_Y34_N16
\DATAPATH|ACTION_MUX2|C[1]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|ACTION_MUX2|C[1]~1_combout\ = (\DATAPATH|REGISTERFITBH|currentState\(1) & (\CONTROL|currentState.Q2~regout\ $ (!\CONTROL|currentState.Q3~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DATAPATH|REGISTERFITBH|currentState\(1),
	datab => \CONTROL|currentState.Q2~regout\,
	datad => \CONTROL|currentState.Q3~regout\,
	combout => \DATAPATH|ACTION_MUX2|C[1]~1_combout\);

-- Location: LCCOMB_X32_Y34_N20
\DATAPATH|MUXF2|C[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|MUXF2|C[2]~2_combout\ = (\DATAPATH|REGISTERFITBH|currentState\(2) & \CONTROL|currentState.Q7~regout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \DATAPATH|REGISTERFITBH|currentState\(2),
	datad => \CONTROL|currentState.Q7~regout\,
	combout => \DATAPATH|MUXF2|C[2]~2_combout\);

-- Location: LCFF_X32_Y34_N21
\DATAPATH|REGISTERF2|currentState[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|MUXF2|C[2]~2_combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	ena => \CONTROL|en_count~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERF2|currentState\(2));

-- Location: LCCOMB_X31_Y34_N4
\DATAPATH|REGISTERFITBH|currentState[2]~20\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|REGISTERFITBH|currentState[2]~20_combout\ = ((\DATAPATH|REGISTERF1|currentState\(2) $ (\DATAPATH|REGISTERF2|currentState\(2) $ (!\DATAPATH|REGISTERFITBH|currentState[1]~19\)))) # (GND)
-- \DATAPATH|REGISTERFITBH|currentState[2]~21\ = CARRY((\DATAPATH|REGISTERF1|currentState\(2) & ((\DATAPATH|REGISTERF2|currentState\(2)) # (!\DATAPATH|REGISTERFITBH|currentState[1]~19\))) # (!\DATAPATH|REGISTERF1|currentState\(2) & 
-- (\DATAPATH|REGISTERF2|currentState\(2) & !\DATAPATH|REGISTERFITBH|currentState[1]~19\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \DATAPATH|REGISTERF1|currentState\(2),
	datab => \DATAPATH|REGISTERF2|currentState\(2),
	datad => VCC,
	cin => \DATAPATH|REGISTERFITBH|currentState[1]~19\,
	combout => \DATAPATH|REGISTERFITBH|currentState[2]~20_combout\,
	cout => \DATAPATH|REGISTERFITBH|currentState[2]~21\);

-- Location: LCFF_X31_Y34_N5
\DATAPATH|REGISTERFITBH|currentState[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|REGISTERFITBH|currentState[2]~20_combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	ena => \CONTROL|currentState.Q5~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERFITBH|currentState\(2));

-- Location: LCCOMB_X32_Y34_N14
\DATAPATH|ACTION_MUX2|C[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|ACTION_MUX2|C[2]~2_combout\ = (\DATAPATH|REGISTERFITBH|currentState\(2) & (\CONTROL|currentState.Q2~regout\ $ (!\CONTROL|currentState.Q3~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \CONTROL|currentState.Q2~regout\,
	datab => \DATAPATH|REGISTERFITBH|currentState\(2),
	datad => \CONTROL|currentState.Q3~regout\,
	combout => \DATAPATH|ACTION_MUX2|C[2]~2_combout\);

-- Location: LCCOMB_X30_Y34_N20
\DATAPATH|MUXF2|C[3]~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|MUXF2|C[3]~3_combout\ = (\CONTROL|currentState.Q7~regout\ & \DATAPATH|REGISTERFITBH|currentState\(3))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \CONTROL|currentState.Q7~regout\,
	datad => \DATAPATH|REGISTERFITBH|currentState\(3),
	combout => \DATAPATH|MUXF2|C[3]~3_combout\);

-- Location: LCFF_X30_Y34_N21
\DATAPATH|REGISTERF2|currentState[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|MUXF2|C[3]~3_combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	ena => \CONTROL|en_count~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERF2|currentState\(3));

-- Location: LCFF_X30_Y34_N7
\DATAPATH|REGISTERF1|currentState[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	sdata => \DATAPATH|REGISTERF2|currentState\(3),
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	sload => VCC,
	ena => \CONTROL|currentState.Q6~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERF1|currentState\(3));

-- Location: LCCOMB_X31_Y34_N6
\DATAPATH|REGISTERFITBH|currentState[3]~22\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|REGISTERFITBH|currentState[3]~22_combout\ = (\DATAPATH|REGISTERF2|currentState\(3) & ((\DATAPATH|REGISTERF1|currentState\(3) & (\DATAPATH|REGISTERFITBH|currentState[2]~21\ & VCC)) # (!\DATAPATH|REGISTERF1|currentState\(3) & 
-- (!\DATAPATH|REGISTERFITBH|currentState[2]~21\)))) # (!\DATAPATH|REGISTERF2|currentState\(3) & ((\DATAPATH|REGISTERF1|currentState\(3) & (!\DATAPATH|REGISTERFITBH|currentState[2]~21\)) # (!\DATAPATH|REGISTERF1|currentState\(3) & 
-- ((\DATAPATH|REGISTERFITBH|currentState[2]~21\) # (GND)))))
-- \DATAPATH|REGISTERFITBH|currentState[3]~23\ = CARRY((\DATAPATH|REGISTERF2|currentState\(3) & (!\DATAPATH|REGISTERF1|currentState\(3) & !\DATAPATH|REGISTERFITBH|currentState[2]~21\)) # (!\DATAPATH|REGISTERF2|currentState\(3) & 
-- ((!\DATAPATH|REGISTERFITBH|currentState[2]~21\) # (!\DATAPATH|REGISTERF1|currentState\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \DATAPATH|REGISTERF2|currentState\(3),
	datab => \DATAPATH|REGISTERF1|currentState\(3),
	datad => VCC,
	cin => \DATAPATH|REGISTERFITBH|currentState[2]~21\,
	combout => \DATAPATH|REGISTERFITBH|currentState[3]~22_combout\,
	cout => \DATAPATH|REGISTERFITBH|currentState[3]~23\);

-- Location: LCFF_X31_Y34_N7
\DATAPATH|REGISTERFITBH|currentState[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|REGISTERFITBH|currentState[3]~22_combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	ena => \CONTROL|currentState.Q5~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERFITBH|currentState\(3));

-- Location: LCCOMB_X30_Y34_N14
\DATAPATH|ACTION_MUX2|C[3]~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|ACTION_MUX2|C[3]~3_combout\ = (\DATAPATH|REGISTERFITBH|currentState\(3) & (\CONTROL|currentState.Q3~regout\ $ (!\CONTROL|currentState.Q2~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \CONTROL|currentState.Q3~regout\,
	datab => \CONTROL|currentState.Q2~regout\,
	datad => \DATAPATH|REGISTERFITBH|currentState\(3),
	combout => \DATAPATH|ACTION_MUX2|C[3]~3_combout\);

-- Location: LCCOMB_X30_Y34_N2
\DATAPATH|MUXF2|C[4]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|MUXF2|C[4]~4_combout\ = (\DATAPATH|REGISTERFITBH|currentState\(4) & \CONTROL|currentState.Q7~regout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \DATAPATH|REGISTERFITBH|currentState\(4),
	datad => \CONTROL|currentState.Q7~regout\,
	combout => \DATAPATH|MUXF2|C[4]~4_combout\);

-- Location: LCFF_X30_Y34_N3
\DATAPATH|REGISTERF2|currentState[4]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|MUXF2|C[4]~4_combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	ena => \CONTROL|en_count~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERF2|currentState\(4));

-- Location: LCFF_X30_Y34_N25
\DATAPATH|REGISTERF1|currentState[4]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	sdata => \DATAPATH|REGISTERF2|currentState\(4),
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	sload => VCC,
	ena => \CONTROL|currentState.Q6~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERF1|currentState\(4));

-- Location: LCCOMB_X31_Y34_N8
\DATAPATH|REGISTERFITBH|currentState[4]~24\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|REGISTERFITBH|currentState[4]~24_combout\ = ((\DATAPATH|REGISTERF2|currentState\(4) $ (\DATAPATH|REGISTERF1|currentState\(4) $ (!\DATAPATH|REGISTERFITBH|currentState[3]~23\)))) # (GND)
-- \DATAPATH|REGISTERFITBH|currentState[4]~25\ = CARRY((\DATAPATH|REGISTERF2|currentState\(4) & ((\DATAPATH|REGISTERF1|currentState\(4)) # (!\DATAPATH|REGISTERFITBH|currentState[3]~23\))) # (!\DATAPATH|REGISTERF2|currentState\(4) & 
-- (\DATAPATH|REGISTERF1|currentState\(4) & !\DATAPATH|REGISTERFITBH|currentState[3]~23\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \DATAPATH|REGISTERF2|currentState\(4),
	datab => \DATAPATH|REGISTERF1|currentState\(4),
	datad => VCC,
	cin => \DATAPATH|REGISTERFITBH|currentState[3]~23\,
	combout => \DATAPATH|REGISTERFITBH|currentState[4]~24_combout\,
	cout => \DATAPATH|REGISTERFITBH|currentState[4]~25\);

-- Location: LCFF_X31_Y34_N9
\DATAPATH|REGISTERFITBH|currentState[4]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|REGISTERFITBH|currentState[4]~24_combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	ena => \CONTROL|currentState.Q5~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERFITBH|currentState\(4));

-- Location: LCCOMB_X30_Y34_N10
\DATAPATH|ACTION_MUX2|C[4]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|ACTION_MUX2|C[4]~4_combout\ = (\DATAPATH|REGISTERFITBH|currentState\(4) & (\CONTROL|currentState.Q3~regout\ $ (!\CONTROL|currentState.Q2~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \CONTROL|currentState.Q3~regout\,
	datab => \DATAPATH|REGISTERFITBH|currentState\(4),
	datad => \CONTROL|currentState.Q2~regout\,
	combout => \DATAPATH|ACTION_MUX2|C[4]~4_combout\);

-- Location: LCCOMB_X30_Y34_N26
\DATAPATH|MUXF2|C[5]~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|MUXF2|C[5]~5_combout\ = (\DATAPATH|REGISTERFITBH|currentState\(5) & \CONTROL|currentState.Q7~regout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DATAPATH|REGISTERFITBH|currentState\(5),
	datad => \CONTROL|currentState.Q7~regout\,
	combout => \DATAPATH|MUXF2|C[5]~5_combout\);

-- Location: LCFF_X30_Y34_N27
\DATAPATH|REGISTERF2|currentState[5]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|MUXF2|C[5]~5_combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	ena => \CONTROL|en_count~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERF2|currentState\(5));

-- Location: LCFF_X30_Y34_N23
\DATAPATH|REGISTERF1|currentState[5]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	sdata => \DATAPATH|REGISTERF2|currentState\(5),
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	sload => VCC,
	ena => \CONTROL|currentState.Q6~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERF1|currentState\(5));

-- Location: LCCOMB_X31_Y34_N10
\DATAPATH|REGISTERFITBH|currentState[5]~26\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|REGISTERFITBH|currentState[5]~26_combout\ = (\DATAPATH|REGISTERF2|currentState\(5) & ((\DATAPATH|REGISTERF1|currentState\(5) & (\DATAPATH|REGISTERFITBH|currentState[4]~25\ & VCC)) # (!\DATAPATH|REGISTERF1|currentState\(5) & 
-- (!\DATAPATH|REGISTERFITBH|currentState[4]~25\)))) # (!\DATAPATH|REGISTERF2|currentState\(5) & ((\DATAPATH|REGISTERF1|currentState\(5) & (!\DATAPATH|REGISTERFITBH|currentState[4]~25\)) # (!\DATAPATH|REGISTERF1|currentState\(5) & 
-- ((\DATAPATH|REGISTERFITBH|currentState[4]~25\) # (GND)))))
-- \DATAPATH|REGISTERFITBH|currentState[5]~27\ = CARRY((\DATAPATH|REGISTERF2|currentState\(5) & (!\DATAPATH|REGISTERF1|currentState\(5) & !\DATAPATH|REGISTERFITBH|currentState[4]~25\)) # (!\DATAPATH|REGISTERF2|currentState\(5) & 
-- ((!\DATAPATH|REGISTERFITBH|currentState[4]~25\) # (!\DATAPATH|REGISTERF1|currentState\(5)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \DATAPATH|REGISTERF2|currentState\(5),
	datab => \DATAPATH|REGISTERF1|currentState\(5),
	datad => VCC,
	cin => \DATAPATH|REGISTERFITBH|currentState[4]~25\,
	combout => \DATAPATH|REGISTERFITBH|currentState[5]~26_combout\,
	cout => \DATAPATH|REGISTERFITBH|currentState[5]~27\);

-- Location: LCFF_X31_Y34_N11
\DATAPATH|REGISTERFITBH|currentState[5]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|REGISTERFITBH|currentState[5]~26_combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	ena => \CONTROL|currentState.Q5~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERFITBH|currentState\(5));

-- Location: LCCOMB_X30_Y34_N24
\DATAPATH|ACTION_MUX2|C[5]~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|ACTION_MUX2|C[5]~5_combout\ = (\DATAPATH|REGISTERFITBH|currentState\(5) & (\CONTROL|currentState.Q3~regout\ $ (!\CONTROL|currentState.Q2~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DATAPATH|REGISTERFITBH|currentState\(5),
	datab => \CONTROL|currentState.Q3~regout\,
	datad => \CONTROL|currentState.Q2~regout\,
	combout => \DATAPATH|ACTION_MUX2|C[5]~5_combout\);

-- Location: LCCOMB_X30_Y34_N0
\DATAPATH|MUXF2|C[6]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|MUXF2|C[6]~6_combout\ = (\DATAPATH|REGISTERFITBH|currentState\(6) & \CONTROL|currentState.Q7~regout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \DATAPATH|REGISTERFITBH|currentState\(6),
	datad => \CONTROL|currentState.Q7~regout\,
	combout => \DATAPATH|MUXF2|C[6]~6_combout\);

-- Location: LCFF_X30_Y34_N1
\DATAPATH|REGISTERF2|currentState[6]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|MUXF2|C[6]~6_combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	ena => \CONTROL|en_count~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERF2|currentState\(6));

-- Location: LCFF_X30_Y34_N29
\DATAPATH|REGISTERF1|currentState[6]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	sdata => \DATAPATH|REGISTERF2|currentState\(6),
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	sload => VCC,
	ena => \CONTROL|currentState.Q6~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERF1|currentState\(6));

-- Location: LCCOMB_X31_Y34_N12
\DATAPATH|REGISTERFITBH|currentState[6]~28\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|REGISTERFITBH|currentState[6]~28_combout\ = ((\DATAPATH|REGISTERF2|currentState\(6) $ (\DATAPATH|REGISTERF1|currentState\(6) $ (!\DATAPATH|REGISTERFITBH|currentState[5]~27\)))) # (GND)
-- \DATAPATH|REGISTERFITBH|currentState[6]~29\ = CARRY((\DATAPATH|REGISTERF2|currentState\(6) & ((\DATAPATH|REGISTERF1|currentState\(6)) # (!\DATAPATH|REGISTERFITBH|currentState[5]~27\))) # (!\DATAPATH|REGISTERF2|currentState\(6) & 
-- (\DATAPATH|REGISTERF1|currentState\(6) & !\DATAPATH|REGISTERFITBH|currentState[5]~27\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \DATAPATH|REGISTERF2|currentState\(6),
	datab => \DATAPATH|REGISTERF1|currentState\(6),
	datad => VCC,
	cin => \DATAPATH|REGISTERFITBH|currentState[5]~27\,
	combout => \DATAPATH|REGISTERFITBH|currentState[6]~28_combout\,
	cout => \DATAPATH|REGISTERFITBH|currentState[6]~29\);

-- Location: LCFF_X31_Y34_N13
\DATAPATH|REGISTERFITBH|currentState[6]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|REGISTERFITBH|currentState[6]~28_combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	ena => \CONTROL|currentState.Q5~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERFITBH|currentState\(6));

-- Location: LCCOMB_X32_Y34_N30
\DATAPATH|ACTION_MUX2|C[6]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|ACTION_MUX2|C[6]~6_combout\ = (\DATAPATH|REGISTERFITBH|currentState\(6) & (\CONTROL|currentState.Q3~regout\ $ (!\CONTROL|currentState.Q2~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DATAPATH|REGISTERFITBH|currentState\(6),
	datac => \CONTROL|currentState.Q3~regout\,
	datad => \CONTROL|currentState.Q2~regout\,
	combout => \DATAPATH|ACTION_MUX2|C[6]~6_combout\);

-- Location: LCCOMB_X30_Y34_N4
\DATAPATH|MUXF2|C[7]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|MUXF2|C[7]~7_combout\ = (\DATAPATH|REGISTERFITBH|currentState\(7) & \CONTROL|currentState.Q7~regout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DATAPATH|REGISTERFITBH|currentState\(7),
	datad => \CONTROL|currentState.Q7~regout\,
	combout => \DATAPATH|MUXF2|C[7]~7_combout\);

-- Location: LCFF_X32_Y34_N5
\DATAPATH|REGISTERF2|currentState[7]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	sdata => \DATAPATH|MUXF2|C[7]~7_combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	sload => VCC,
	ena => \CONTROL|en_count~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERF2|currentState\(7));

-- Location: LCFF_X32_Y34_N15
\DATAPATH|REGISTERF1|currentState[7]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	sdata => \DATAPATH|REGISTERF2|currentState\(7),
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	sload => VCC,
	ena => \CONTROL|currentState.Q6~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERF1|currentState\(7));

-- Location: LCCOMB_X31_Y34_N14
\DATAPATH|REGISTERFITBH|currentState[7]~30\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|REGISTERFITBH|currentState[7]~30_combout\ = (\DATAPATH|REGISTERF2|currentState\(7) & ((\DATAPATH|REGISTERF1|currentState\(7) & (\DATAPATH|REGISTERFITBH|currentState[6]~29\ & VCC)) # (!\DATAPATH|REGISTERF1|currentState\(7) & 
-- (!\DATAPATH|REGISTERFITBH|currentState[6]~29\)))) # (!\DATAPATH|REGISTERF2|currentState\(7) & ((\DATAPATH|REGISTERF1|currentState\(7) & (!\DATAPATH|REGISTERFITBH|currentState[6]~29\)) # (!\DATAPATH|REGISTERF1|currentState\(7) & 
-- ((\DATAPATH|REGISTERFITBH|currentState[6]~29\) # (GND)))))
-- \DATAPATH|REGISTERFITBH|currentState[7]~31\ = CARRY((\DATAPATH|REGISTERF2|currentState\(7) & (!\DATAPATH|REGISTERF1|currentState\(7) & !\DATAPATH|REGISTERFITBH|currentState[6]~29\)) # (!\DATAPATH|REGISTERF2|currentState\(7) & 
-- ((!\DATAPATH|REGISTERFITBH|currentState[6]~29\) # (!\DATAPATH|REGISTERF1|currentState\(7)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \DATAPATH|REGISTERF2|currentState\(7),
	datab => \DATAPATH|REGISTERF1|currentState\(7),
	datad => VCC,
	cin => \DATAPATH|REGISTERFITBH|currentState[6]~29\,
	combout => \DATAPATH|REGISTERFITBH|currentState[7]~30_combout\,
	cout => \DATAPATH|REGISTERFITBH|currentState[7]~31\);

-- Location: LCFF_X31_Y34_N15
\DATAPATH|REGISTERFITBH|currentState[7]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|REGISTERFITBH|currentState[7]~30_combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	ena => \CONTROL|currentState.Q5~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERFITBH|currentState\(7));

-- Location: LCCOMB_X30_Y34_N16
\DATAPATH|ACTION_MUX2|C[7]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|ACTION_MUX2|C[7]~7_combout\ = (\DATAPATH|REGISTERFITBH|currentState\(7) & (\CONTROL|currentState.Q3~regout\ $ (!\CONTROL|currentState.Q2~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \CONTROL|currentState.Q3~regout\,
	datab => \DATAPATH|REGISTERFITBH|currentState\(7),
	datad => \CONTROL|currentState.Q2~regout\,
	combout => \DATAPATH|ACTION_MUX2|C[7]~7_combout\);

-- Location: LCCOMB_X30_Y34_N18
\DATAPATH|MUXF2|C[8]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|MUXF2|C[8]~8_combout\ = (\DATAPATH|REGISTERFITBH|currentState\(8) & \CONTROL|currentState.Q7~regout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \DATAPATH|REGISTERFITBH|currentState\(8),
	datad => \CONTROL|currentState.Q7~regout\,
	combout => \DATAPATH|MUXF2|C[8]~8_combout\);

-- Location: LCFF_X30_Y34_N19
\DATAPATH|REGISTERF2|currentState[8]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|MUXF2|C[8]~8_combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	ena => \CONTROL|en_count~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERF2|currentState\(8));

-- Location: LCCOMB_X31_Y34_N16
\DATAPATH|REGISTERFITBH|currentState[8]~32\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|REGISTERFITBH|currentState[8]~32_combout\ = ((\DATAPATH|REGISTERF1|currentState\(8) $ (\DATAPATH|REGISTERF2|currentState\(8) $ (!\DATAPATH|REGISTERFITBH|currentState[7]~31\)))) # (GND)
-- \DATAPATH|REGISTERFITBH|currentState[8]~33\ = CARRY((\DATAPATH|REGISTERF1|currentState\(8) & ((\DATAPATH|REGISTERF2|currentState\(8)) # (!\DATAPATH|REGISTERFITBH|currentState[7]~31\))) # (!\DATAPATH|REGISTERF1|currentState\(8) & 
-- (\DATAPATH|REGISTERF2|currentState\(8) & !\DATAPATH|REGISTERFITBH|currentState[7]~31\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \DATAPATH|REGISTERF1|currentState\(8),
	datab => \DATAPATH|REGISTERF2|currentState\(8),
	datad => VCC,
	cin => \DATAPATH|REGISTERFITBH|currentState[7]~31\,
	combout => \DATAPATH|REGISTERFITBH|currentState[8]~32_combout\,
	cout => \DATAPATH|REGISTERFITBH|currentState[8]~33\);

-- Location: LCFF_X31_Y34_N17
\DATAPATH|REGISTERFITBH|currentState[8]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|REGISTERFITBH|currentState[8]~32_combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	ena => \CONTROL|currentState.Q5~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERFITBH|currentState\(8));

-- Location: LCCOMB_X30_Y34_N6
\DATAPATH|ACTION_MUX2|C[8]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|ACTION_MUX2|C[8]~8_combout\ = (\DATAPATH|REGISTERFITBH|currentState\(8) & (\CONTROL|currentState.Q3~regout\ $ (!\CONTROL|currentState.Q2~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DATAPATH|REGISTERFITBH|currentState\(8),
	datab => \CONTROL|currentState.Q3~regout\,
	datad => \CONTROL|currentState.Q2~regout\,
	combout => \DATAPATH|ACTION_MUX2|C[8]~8_combout\);

-- Location: LCCOMB_X30_Y34_N12
\DATAPATH|MUXF2|C[9]~9\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|MUXF2|C[9]~9_combout\ = (\CONTROL|currentState.Q7~regout\ & \DATAPATH|REGISTERFITBH|currentState\(9))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \CONTROL|currentState.Q7~regout\,
	datad => \DATAPATH|REGISTERFITBH|currentState\(9),
	combout => \DATAPATH|MUXF2|C[9]~9_combout\);

-- Location: LCFF_X32_Y34_N27
\DATAPATH|REGISTERF2|currentState[9]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	sdata => \DATAPATH|MUXF2|C[9]~9_combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	sload => VCC,
	ena => \CONTROL|en_count~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERF2|currentState\(9));

-- Location: LCFF_X32_Y34_N9
\DATAPATH|REGISTERF1|currentState[9]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	sdata => \DATAPATH|REGISTERF2|currentState\(9),
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	sload => VCC,
	ena => \CONTROL|currentState.Q6~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERF1|currentState\(9));

-- Location: LCCOMB_X31_Y34_N18
\DATAPATH|REGISTERFITBH|currentState[9]~34\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|REGISTERFITBH|currentState[9]~34_combout\ = (\DATAPATH|REGISTERF2|currentState\(9) & ((\DATAPATH|REGISTERF1|currentState\(9) & (\DATAPATH|REGISTERFITBH|currentState[8]~33\ & VCC)) # (!\DATAPATH|REGISTERF1|currentState\(9) & 
-- (!\DATAPATH|REGISTERFITBH|currentState[8]~33\)))) # (!\DATAPATH|REGISTERF2|currentState\(9) & ((\DATAPATH|REGISTERF1|currentState\(9) & (!\DATAPATH|REGISTERFITBH|currentState[8]~33\)) # (!\DATAPATH|REGISTERF1|currentState\(9) & 
-- ((\DATAPATH|REGISTERFITBH|currentState[8]~33\) # (GND)))))
-- \DATAPATH|REGISTERFITBH|currentState[9]~35\ = CARRY((\DATAPATH|REGISTERF2|currentState\(9) & (!\DATAPATH|REGISTERF1|currentState\(9) & !\DATAPATH|REGISTERFITBH|currentState[8]~33\)) # (!\DATAPATH|REGISTERF2|currentState\(9) & 
-- ((!\DATAPATH|REGISTERFITBH|currentState[8]~33\) # (!\DATAPATH|REGISTERF1|currentState\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \DATAPATH|REGISTERF2|currentState\(9),
	datab => \DATAPATH|REGISTERF1|currentState\(9),
	datad => VCC,
	cin => \DATAPATH|REGISTERFITBH|currentState[8]~33\,
	combout => \DATAPATH|REGISTERFITBH|currentState[9]~34_combout\,
	cout => \DATAPATH|REGISTERFITBH|currentState[9]~35\);

-- Location: LCFF_X31_Y34_N19
\DATAPATH|REGISTERFITBH|currentState[9]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|REGISTERFITBH|currentState[9]~34_combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	ena => \CONTROL|currentState.Q5~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERFITBH|currentState\(9));

-- Location: LCCOMB_X32_Y34_N2
\DATAPATH|ACTION_MUX2|C[9]~9\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|ACTION_MUX2|C[9]~9_combout\ = (\DATAPATH|REGISTERFITBH|currentState\(9) & (\CONTROL|currentState.Q2~regout\ $ (!\CONTROL|currentState.Q3~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \CONTROL|currentState.Q2~regout\,
	datab => \CONTROL|currentState.Q3~regout\,
	datad => \DATAPATH|REGISTERFITBH|currentState\(9),
	combout => \DATAPATH|ACTION_MUX2|C[9]~9_combout\);

-- Location: LCCOMB_X32_Y34_N28
\DATAPATH|MUXF2|C[10]~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|MUXF2|C[10]~10_combout\ = (\DATAPATH|REGISTERFITBH|currentState\(10) & \CONTROL|currentState.Q7~regout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \DATAPATH|REGISTERFITBH|currentState\(10),
	datad => \CONTROL|currentState.Q7~regout\,
	combout => \DATAPATH|MUXF2|C[10]~10_combout\);

-- Location: LCFF_X32_Y34_N29
\DATAPATH|REGISTERF2|currentState[10]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|MUXF2|C[10]~10_combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	ena => \CONTROL|en_count~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERF2|currentState\(10));

-- Location: LCCOMB_X31_Y34_N20
\DATAPATH|REGISTERFITBH|currentState[10]~36\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|REGISTERFITBH|currentState[10]~36_combout\ = ((\DATAPATH|REGISTERF1|currentState\(10) $ (\DATAPATH|REGISTERF2|currentState\(10) $ (!\DATAPATH|REGISTERFITBH|currentState[9]~35\)))) # (GND)
-- \DATAPATH|REGISTERFITBH|currentState[10]~37\ = CARRY((\DATAPATH|REGISTERF1|currentState\(10) & ((\DATAPATH|REGISTERF2|currentState\(10)) # (!\DATAPATH|REGISTERFITBH|currentState[9]~35\))) # (!\DATAPATH|REGISTERF1|currentState\(10) & 
-- (\DATAPATH|REGISTERF2|currentState\(10) & !\DATAPATH|REGISTERFITBH|currentState[9]~35\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \DATAPATH|REGISTERF1|currentState\(10),
	datab => \DATAPATH|REGISTERF2|currentState\(10),
	datad => VCC,
	cin => \DATAPATH|REGISTERFITBH|currentState[9]~35\,
	combout => \DATAPATH|REGISTERFITBH|currentState[10]~36_combout\,
	cout => \DATAPATH|REGISTERFITBH|currentState[10]~37\);

-- Location: LCFF_X31_Y34_N21
\DATAPATH|REGISTERFITBH|currentState[10]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|REGISTERFITBH|currentState[10]~36_combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	ena => \CONTROL|currentState.Q5~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERFITBH|currentState\(10));

-- Location: LCCOMB_X30_Y34_N8
\DATAPATH|ACTION_MUX2|C[10]~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|ACTION_MUX2|C[10]~10_combout\ = (\DATAPATH|REGISTERFITBH|currentState\(10) & (\CONTROL|currentState.Q3~regout\ $ (!\CONTROL|currentState.Q2~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \CONTROL|currentState.Q3~regout\,
	datab => \DATAPATH|REGISTERFITBH|currentState\(10),
	datad => \CONTROL|currentState.Q2~regout\,
	combout => \DATAPATH|ACTION_MUX2|C[10]~10_combout\);

-- Location: LCCOMB_X32_Y34_N24
\DATAPATH|MUXF2|C[11]~11\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|MUXF2|C[11]~11_combout\ = (\DATAPATH|REGISTERFITBH|currentState\(11) & \CONTROL|currentState.Q7~regout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \DATAPATH|REGISTERFITBH|currentState\(11),
	datad => \CONTROL|currentState.Q7~regout\,
	combout => \DATAPATH|MUXF2|C[11]~11_combout\);

-- Location: LCFF_X32_Y34_N25
\DATAPATH|REGISTERF2|currentState[11]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|MUXF2|C[11]~11_combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	ena => \CONTROL|en_count~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERF2|currentState\(11));

-- Location: LCCOMB_X31_Y34_N22
\DATAPATH|REGISTERFITBH|currentState[11]~38\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|REGISTERFITBH|currentState[11]~38_combout\ = (\DATAPATH|REGISTERF1|currentState\(11) & ((\DATAPATH|REGISTERF2|currentState\(11) & (\DATAPATH|REGISTERFITBH|currentState[10]~37\ & VCC)) # (!\DATAPATH|REGISTERF2|currentState\(11) & 
-- (!\DATAPATH|REGISTERFITBH|currentState[10]~37\)))) # (!\DATAPATH|REGISTERF1|currentState\(11) & ((\DATAPATH|REGISTERF2|currentState\(11) & (!\DATAPATH|REGISTERFITBH|currentState[10]~37\)) # (!\DATAPATH|REGISTERF2|currentState\(11) & 
-- ((\DATAPATH|REGISTERFITBH|currentState[10]~37\) # (GND)))))
-- \DATAPATH|REGISTERFITBH|currentState[11]~39\ = CARRY((\DATAPATH|REGISTERF1|currentState\(11) & (!\DATAPATH|REGISTERF2|currentState\(11) & !\DATAPATH|REGISTERFITBH|currentState[10]~37\)) # (!\DATAPATH|REGISTERF1|currentState\(11) & 
-- ((!\DATAPATH|REGISTERFITBH|currentState[10]~37\) # (!\DATAPATH|REGISTERF2|currentState\(11)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \DATAPATH|REGISTERF1|currentState\(11),
	datab => \DATAPATH|REGISTERF2|currentState\(11),
	datad => VCC,
	cin => \DATAPATH|REGISTERFITBH|currentState[10]~37\,
	combout => \DATAPATH|REGISTERFITBH|currentState[11]~38_combout\,
	cout => \DATAPATH|REGISTERFITBH|currentState[11]~39\);

-- Location: LCFF_X31_Y34_N23
\DATAPATH|REGISTERFITBH|currentState[11]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|REGISTERFITBH|currentState[11]~38_combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	ena => \CONTROL|currentState.Q5~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERFITBH|currentState\(11));

-- Location: LCCOMB_X32_Y34_N22
\DATAPATH|ACTION_MUX2|C[11]~11\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|ACTION_MUX2|C[11]~11_combout\ = (\DATAPATH|REGISTERFITBH|currentState\(11) & (\CONTROL|currentState.Q3~regout\ $ (!\CONTROL|currentState.Q2~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \CONTROL|currentState.Q3~regout\,
	datac => \DATAPATH|REGISTERFITBH|currentState\(11),
	datad => \CONTROL|currentState.Q2~regout\,
	combout => \DATAPATH|ACTION_MUX2|C[11]~11_combout\);

-- Location: LCCOMB_X32_Y34_N0
\DATAPATH|MUXF2|C[12]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|MUXF2|C[12]~12_combout\ = (\DATAPATH|REGISTERFITBH|currentState\(12) & \CONTROL|currentState.Q7~regout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DATAPATH|REGISTERFITBH|currentState\(12),
	datad => \CONTROL|currentState.Q7~regout\,
	combout => \DATAPATH|MUXF2|C[12]~12_combout\);

-- Location: LCFF_X32_Y34_N1
\DATAPATH|REGISTERF2|currentState[12]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|MUXF2|C[12]~12_combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	ena => \CONTROL|en_count~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERF2|currentState\(12));

-- Location: LCCOMB_X31_Y34_N24
\DATAPATH|REGISTERFITBH|currentState[12]~40\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|REGISTERFITBH|currentState[12]~40_combout\ = ((\DATAPATH|REGISTERF1|currentState\(12) $ (\DATAPATH|REGISTERF2|currentState\(12) $ (!\DATAPATH|REGISTERFITBH|currentState[11]~39\)))) # (GND)
-- \DATAPATH|REGISTERFITBH|currentState[12]~41\ = CARRY((\DATAPATH|REGISTERF1|currentState\(12) & ((\DATAPATH|REGISTERF2|currentState\(12)) # (!\DATAPATH|REGISTERFITBH|currentState[11]~39\))) # (!\DATAPATH|REGISTERF1|currentState\(12) & 
-- (\DATAPATH|REGISTERF2|currentState\(12) & !\DATAPATH|REGISTERFITBH|currentState[11]~39\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \DATAPATH|REGISTERF1|currentState\(12),
	datab => \DATAPATH|REGISTERF2|currentState\(12),
	datad => VCC,
	cin => \DATAPATH|REGISTERFITBH|currentState[11]~39\,
	combout => \DATAPATH|REGISTERFITBH|currentState[12]~40_combout\,
	cout => \DATAPATH|REGISTERFITBH|currentState[12]~41\);

-- Location: LCFF_X31_Y34_N25
\DATAPATH|REGISTERFITBH|currentState[12]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|REGISTERFITBH|currentState[12]~40_combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	ena => \CONTROL|currentState.Q5~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERFITBH|currentState\(12));

-- Location: LCCOMB_X32_Y34_N12
\DATAPATH|ACTION_MUX2|C[12]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|ACTION_MUX2|C[12]~12_combout\ = (\DATAPATH|REGISTERFITBH|currentState\(12) & (\CONTROL|currentState.Q2~regout\ $ (!\CONTROL|currentState.Q3~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \CONTROL|currentState.Q2~regout\,
	datab => \CONTROL|currentState.Q3~regout\,
	datad => \DATAPATH|REGISTERFITBH|currentState\(12),
	combout => \DATAPATH|ACTION_MUX2|C[12]~12_combout\);

-- Location: LCCOMB_X29_Y34_N20
\DATAPATH|MUXF2|C[13]~13\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|MUXF2|C[13]~13_combout\ = (\CONTROL|currentState.Q7~regout\ & \DATAPATH|REGISTERFITBH|currentState\(13))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \CONTROL|currentState.Q7~regout\,
	datad => \DATAPATH|REGISTERFITBH|currentState\(13),
	combout => \DATAPATH|MUXF2|C[13]~13_combout\);

-- Location: LCFF_X29_Y34_N21
\DATAPATH|REGISTERF2|currentState[13]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|MUXF2|C[13]~13_combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	ena => \CONTROL|en_count~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERF2|currentState\(13));

-- Location: LCFF_X30_Y34_N5
\DATAPATH|REGISTERF1|currentState[13]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	sdata => \DATAPATH|REGISTERF2|currentState\(13),
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	sload => VCC,
	ena => \CONTROL|currentState.Q6~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERF1|currentState\(13));

-- Location: LCCOMB_X31_Y34_N26
\DATAPATH|REGISTERFITBH|currentState[13]~42\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|REGISTERFITBH|currentState[13]~42_combout\ = (\DATAPATH|REGISTERF2|currentState\(13) & ((\DATAPATH|REGISTERF1|currentState\(13) & (\DATAPATH|REGISTERFITBH|currentState[12]~41\ & VCC)) # (!\DATAPATH|REGISTERF1|currentState\(13) & 
-- (!\DATAPATH|REGISTERFITBH|currentState[12]~41\)))) # (!\DATAPATH|REGISTERF2|currentState\(13) & ((\DATAPATH|REGISTERF1|currentState\(13) & (!\DATAPATH|REGISTERFITBH|currentState[12]~41\)) # (!\DATAPATH|REGISTERF1|currentState\(13) & 
-- ((\DATAPATH|REGISTERFITBH|currentState[12]~41\) # (GND)))))
-- \DATAPATH|REGISTERFITBH|currentState[13]~43\ = CARRY((\DATAPATH|REGISTERF2|currentState\(13) & (!\DATAPATH|REGISTERF1|currentState\(13) & !\DATAPATH|REGISTERFITBH|currentState[12]~41\)) # (!\DATAPATH|REGISTERF2|currentState\(13) & 
-- ((!\DATAPATH|REGISTERFITBH|currentState[12]~41\) # (!\DATAPATH|REGISTERF1|currentState\(13)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \DATAPATH|REGISTERF2|currentState\(13),
	datab => \DATAPATH|REGISTERF1|currentState\(13),
	datad => VCC,
	cin => \DATAPATH|REGISTERFITBH|currentState[12]~41\,
	combout => \DATAPATH|REGISTERFITBH|currentState[13]~42_combout\,
	cout => \DATAPATH|REGISTERFITBH|currentState[13]~43\);

-- Location: LCFF_X31_Y34_N27
\DATAPATH|REGISTERFITBH|currentState[13]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|REGISTERFITBH|currentState[13]~42_combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	ena => \CONTROL|currentState.Q5~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERFITBH|currentState\(13));

-- Location: LCCOMB_X32_Y34_N8
\DATAPATH|ACTION_MUX2|C[13]~13\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|ACTION_MUX2|C[13]~13_combout\ = (\DATAPATH|REGISTERFITBH|currentState\(13) & (\CONTROL|currentState.Q2~regout\ $ (!\CONTROL|currentState.Q3~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \CONTROL|currentState.Q2~regout\,
	datab => \CONTROL|currentState.Q3~regout\,
	datad => \DATAPATH|REGISTERFITBH|currentState\(13),
	combout => \DATAPATH|ACTION_MUX2|C[13]~13_combout\);

-- Location: LCCOMB_X32_Y34_N6
\DATAPATH|MUXF2|C[14]~14\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|MUXF2|C[14]~14_combout\ = (\DATAPATH|REGISTERFITBH|currentState\(14) & \CONTROL|currentState.Q7~regout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \DATAPATH|REGISTERFITBH|currentState\(14),
	datad => \CONTROL|currentState.Q7~regout\,
	combout => \DATAPATH|MUXF2|C[14]~14_combout\);

-- Location: LCFF_X32_Y34_N7
\DATAPATH|REGISTERF2|currentState[14]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|MUXF2|C[14]~14_combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	ena => \CONTROL|en_count~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERF2|currentState\(14));

-- Location: LCFF_X32_Y34_N13
\DATAPATH|REGISTERF1|currentState[14]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	sdata => \DATAPATH|REGISTERF2|currentState\(14),
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	sload => VCC,
	ena => \CONTROL|currentState.Q6~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERF1|currentState\(14));

-- Location: LCCOMB_X31_Y34_N28
\DATAPATH|REGISTERFITBH|currentState[14]~44\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|REGISTERFITBH|currentState[14]~44_combout\ = ((\DATAPATH|REGISTERF2|currentState\(14) $ (\DATAPATH|REGISTERF1|currentState\(14) $ (!\DATAPATH|REGISTERFITBH|currentState[13]~43\)))) # (GND)
-- \DATAPATH|REGISTERFITBH|currentState[14]~45\ = CARRY((\DATAPATH|REGISTERF2|currentState\(14) & ((\DATAPATH|REGISTERF1|currentState\(14)) # (!\DATAPATH|REGISTERFITBH|currentState[13]~43\))) # (!\DATAPATH|REGISTERF2|currentState\(14) & 
-- (\DATAPATH|REGISTERF1|currentState\(14) & !\DATAPATH|REGISTERFITBH|currentState[13]~43\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \DATAPATH|REGISTERF2|currentState\(14),
	datab => \DATAPATH|REGISTERF1|currentState\(14),
	datad => VCC,
	cin => \DATAPATH|REGISTERFITBH|currentState[13]~43\,
	combout => \DATAPATH|REGISTERFITBH|currentState[14]~44_combout\,
	cout => \DATAPATH|REGISTERFITBH|currentState[14]~45\);

-- Location: LCFF_X31_Y34_N29
\DATAPATH|REGISTERFITBH|currentState[14]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|REGISTERFITBH|currentState[14]~44_combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	ena => \CONTROL|currentState.Q5~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERFITBH|currentState\(14));

-- Location: LCCOMB_X32_Y34_N4
\DATAPATH|ACTION_MUX2|C[14]~14\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|ACTION_MUX2|C[14]~14_combout\ = (\DATAPATH|REGISTERFITBH|currentState\(14) & (\CONTROL|currentState.Q2~regout\ $ (!\CONTROL|currentState.Q3~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \CONTROL|currentState.Q2~regout\,
	datab => \CONTROL|currentState.Q3~regout\,
	datad => \DATAPATH|REGISTERFITBH|currentState\(14),
	combout => \DATAPATH|ACTION_MUX2|C[14]~14_combout\);

-- Location: LCCOMB_X32_Y34_N10
\DATAPATH|MUXF2|C[15]~15\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|MUXF2|C[15]~15_combout\ = (\DATAPATH|REGISTERFITBH|currentState\(15) & \CONTROL|currentState.Q7~regout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DATAPATH|REGISTERFITBH|currentState\(15),
	datad => \CONTROL|currentState.Q7~regout\,
	combout => \DATAPATH|MUXF2|C[15]~15_combout\);

-- Location: LCFF_X29_Y34_N31
\DATAPATH|REGISTERF2|currentState[15]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	sdata => \DATAPATH|MUXF2|C[15]~15_combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	sload => VCC,
	ena => \CONTROL|en_count~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERF2|currentState\(15));

-- Location: LCFF_X32_Y34_N11
\DATAPATH|REGISTERF1|currentState[15]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	sdata => \DATAPATH|REGISTERF2|currentState\(15),
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	sload => VCC,
	ena => \CONTROL|currentState.Q6~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERF1|currentState\(15));

-- Location: LCCOMB_X31_Y34_N30
\DATAPATH|REGISTERFITBH|currentState[15]~46\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|REGISTERFITBH|currentState[15]~46_combout\ = \DATAPATH|REGISTERF1|currentState\(15) $ (\DATAPATH|REGISTERFITBH|currentState[14]~45\ $ (\DATAPATH|REGISTERF2|currentState\(15)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \DATAPATH|REGISTERF1|currentState\(15),
	datad => \DATAPATH|REGISTERF2|currentState\(15),
	cin => \DATAPATH|REGISTERFITBH|currentState[14]~45\,
	combout => \DATAPATH|REGISTERFITBH|currentState[15]~46_combout\);

-- Location: LCFF_X31_Y34_N31
\DATAPATH|REGISTERFITBH|currentState[15]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \DATAPATH|REGISTERFITBH|currentState[15]~46_combout\,
	aclr => \CONTROL|ALT_INV_currentState.Q0~regout\,
	ena => \CONTROL|currentState.Q5~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DATAPATH|REGISTERFITBH|currentState\(15));

-- Location: LCCOMB_X30_Y34_N28
\DATAPATH|ACTION_MUX2|C[15]~15\ : cycloneii_lcell_comb
-- Equation(s):
-- \DATAPATH|ACTION_MUX2|C[15]~15_combout\ = (\DATAPATH|REGISTERFITBH|currentState\(15) & (\CONTROL|currentState.Q3~regout\ $ (!\CONTROL|currentState.Q2~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \CONTROL|currentState.Q3~regout\,
	datab => \DATAPATH|REGISTERFITBH|currentState\(15),
	datad => \CONTROL|currentState.Q2~regout\,
	combout => \DATAPATH|ACTION_MUX2|C[15]~15_combout\);

-- Location: PIN_P2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\clk~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_clk,
	combout => \clk~combout\);

-- Location: CLKCTRL_G3
\clk~clkctrl\ : cycloneii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \clk~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \clk~clkctrl_outclk\);

-- Location: LCCOMB_X25_Y34_N0
\CONTROL|Selector8~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \CONTROL|Selector8~0_combout\ = (\CONTROL|currentState.Q7~regout\ & (\DATAPATH|COUNTER_COMPARATOR|Equal0~9_combout\ & \DATAPATH|COUNTER_COMPARATOR|Equal0~4_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \CONTROL|currentState.Q7~regout\,
	datac => \DATAPATH|COUNTER_COMPARATOR|Equal0~9_combout\,
	datad => \DATAPATH|COUNTER_COMPARATOR|Equal0~4_combout\,
	combout => \CONTROL|Selector8~0_combout\);

-- Location: LCFF_X25_Y34_N1
\CONTROL|currentState.Q8\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \CONTROL|Selector8~0_combout\,
	aclr => \rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \CONTROL|currentState.Q8~regout\);

-- Location: LCCOMB_X25_Y34_N20
\CONTROL|currentState.Q9~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \CONTROL|currentState.Q9~0_combout\ = (\CONTROL|currentState.Q9~regout\) # (\CONTROL|currentState.Q8~regout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \CONTROL|currentState.Q9~regout\,
	datad => \CONTROL|currentState.Q8~regout\,
	combout => \CONTROL|currentState.Q9~0_combout\);

-- Location: LCFF_X25_Y34_N21
\CONTROL|currentState.Q9\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \CONTROL|currentState.Q9~0_combout\,
	aclr => \rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \CONTROL|currentState.Q9~regout\);

-- Location: PIN_C16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\R[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \DATAPATH|ACTION_MUX2|C[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_R(0));

-- Location: PIN_G13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\R[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \DATAPATH|ACTION_MUX2|C[1]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_R(1));

-- Location: PIN_B14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\R[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \DATAPATH|ACTION_MUX2|C[2]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_R(2));

-- Location: PIN_C12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\R[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \DATAPATH|ACTION_MUX2|C[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_R(3));

-- Location: PIN_G14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\R[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \DATAPATH|ACTION_MUX2|C[4]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_R(4));

-- Location: PIN_B12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\R[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \DATAPATH|ACTION_MUX2|C[5]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_R(5));

-- Location: PIN_F13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\R[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \DATAPATH|ACTION_MUX2|C[6]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_R(6));

-- Location: PIN_A14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\R[7]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \DATAPATH|ACTION_MUX2|C[7]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_R(7));

-- Location: PIN_C15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\R[8]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \DATAPATH|ACTION_MUX2|C[8]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_R(8));

-- Location: PIN_D16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\R[9]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \DATAPATH|ACTION_MUX2|C[9]~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_R(9));

-- Location: PIN_C11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\R[10]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \DATAPATH|ACTION_MUX2|C[10]~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_R(10));

-- Location: PIN_B16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\R[11]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \DATAPATH|ACTION_MUX2|C[11]~11_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_R(11));

-- Location: PIN_B15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\R[12]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \DATAPATH|ACTION_MUX2|C[12]~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_R(12));

-- Location: PIN_F14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\R[13]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \DATAPATH|ACTION_MUX2|C[13]~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_R(13));

-- Location: PIN_D15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\R[14]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \DATAPATH|ACTION_MUX2|C[14]~14_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_R(14));

-- Location: PIN_D14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\R[15]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \DATAPATH|ACTION_MUX2|C[15]~15_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_R(15));

-- Location: PIN_C10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\ready~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \CONTROL|currentState.Q9~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_ready);
END structure;



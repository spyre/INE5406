force /clk 0 0ns, 1 10ns -r 100ns

force /reset 0 0ns

force /N 5 0ns

force /sel_f2 1 700ns

force /en_f1 1 600ns

force /en_f2 1 400ns, 1 700ns

force /en_fibth 1 500ns

force /sel_cond1 1 200ns

force /sel_cond2 1 200ns, 1 300ns

force /en_count 1 400ns, 1 700ns

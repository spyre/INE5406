library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity NBIT_ADDER is
	generic (BUS_WIDTH: POSITIVE := 32);
	port
	(
		-- Input ports
		A: in std_logic_vector(BUS_WIDTH - 1 downto 0);
		B: in std_logic_vector(BUS_WIDTH - 1 downto 0);
		
		-- Output ports
		C: out std_logic_vector(BUS_WIDTH - 1 downto 0)
	);
end entity;

architecture bhv of NBIT_ADDER is
begin

	C <= std_logic_vector(unsigned(A) + unsigned(B));

end architecture;

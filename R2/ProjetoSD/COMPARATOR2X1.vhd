--usar not xor

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity COMPARATOR2X1 is 
generic(BUS_WIDTH: POSITIVE := 32);
port(A: in std_logic_vector(BUS_WIDTH-1 downto 0);
	  B: in std_logic_vector(BUS_WIDTH-1 downto 0);
	  C: out std_logic
	  );
	  
end entity;

architecture bhv of COMPARATOR2X1 is
begin

C <= '1' when (unsigned(A) = unsigned(B)) else '0';

end architecture;
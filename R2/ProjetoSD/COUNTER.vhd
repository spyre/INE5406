
library ieee;
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all;

entity COUNTER is
generic(BUS_WIDTH: POSITIVE := 5);
port(COUNTER_OUTPUT: out std_logic_vector(BUS_WIDTH-1 downto 0); 
	  COUNTER_LIMIT: in std_logic_vector(BUS_WIDTH-1 downto 0); -- how many you wanna count
	  COUNTER_STOP: in std_logic; -- you can keep the COUNTER_LIMITE value stuck after the counter reached its value
	  
	  
	  ENABLE: in std_logic;
	  RESET: in std_logic;
	  CLOCK: in std_logic
	  );
	  
end entity;

architecture bhv of COUNTER is
subtype InternalState is std_logic_vector(BUS_WIDTH-1 downto 0);
signal nextState, currentState: InternalState;

signal COUNT: std_logic_vector(BUS_WIDTH-1 downto 0);
signal TEMP: unsigned(BUS_WIDTH-1 downto 0);

begin
----------------------------------------------------------------------------------
--NEXT STATE LOGIC
	NSL: nextState <= std_logic_vector(unsigned(currentState) + TEMP) ;
----------------------------------------------------------------------------------
----------------------------------------------------------------------------------
TEMP <= to_unsigned(1, TEMP'length);
----------------------------------------------------------------------------------
--SEQUENTIAL LOGIC
SL:	
	process(CLOCK, RESET, ENABLE, currentState,COUNTER_LIMIT, COUNTER_STOP) is
	
	begin
	
		if (COUNTER_STOP = '1' and currentState = COUNTER_LIMIT) then
				currentState <= currentState;
		elsif (RESET = '1' or currentState = COUNTER_LIMIT) then
			currentState <= (others => '0');
		elsif rising_edge(CLOCK) then 
			if ENABLE = '1' then
				currentState <= nextState;
			end if;
		end if;
	end process;
----------------------------------------------------------------------------------
----------------------------------------------------------------------------------
-- OUTPUT LOGIC 
COUNTER_OUTPUT <= currentState;
----------------------------------------------------------------------------------
end architecture;


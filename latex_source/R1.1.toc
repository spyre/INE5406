\changetocdepth {4}
\select@language {brazil}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{Introdu\IeC {\c c}\IeC {\~a}o}{9}{chapter*.2}
\contentsline {part}{\partnumberline {I}\MakeTextUppercase {Projeto do Sistema}}{11}{part.1}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {Identifica\IeC {\c c}\IeC {\~a}o de Entradas e Sa\IeC {\'\i }das}}{13}{chapter.1}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}\MakeTextUppercase {Descri\IeC {\c c}\IeC {\~a}o e Captura Comportamental}}{15}{chapter.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {Projeto do Bloco Operativo}}{19}{chapter.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {4}\MakeTextUppercase {Projeto do Bloco de Controle}}{23}{chapter.4}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {5}\MakeTextUppercase {Projeto da Unidade L\IeC {\'o}gico-Aritm\IeC {\'e}tica}}{25}{chapter.5}
\contentsline {part}{\partnumberline {II}\MakeTextUppercase {Desenvolvimento e Teste de Componentes}}{27}{part.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {6}\MakeTextUppercase {\relax \fontsize {14.4}{18}\selectfont Escrevendo os componentes}}{29}{chapter.6}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {7}\MakeTextUppercase {Testando os Componentes}}{47}{chapter.7}
\contentsline {part}{\partnumberline {III}\MakeTextUppercase {Adapta\IeC {\c c}\IeC {\~o}es e funcionamento}}{51}{part.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {8}\MakeTextUppercase {Adapta\IeC {\c c}\IeC {\~a}o direta para a placa DE2}}{53}{chapter.8}
\contentsline {part}{\partnumberline {IV}\MakeTextUppercase {Conclus\IeC {\~a}o}}{59}{part.4}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {9}\MakeTextUppercase {Considera\IeC {\c c}\IeC {\~o}es finais}}{61}{chapter.9}
\vspace {\cftbeforechapterskip }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{\uppercase {Refer\^encias}}{65}{section*.9}

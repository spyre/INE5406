\select@language {brazil}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{Introdu\IeC {\c c}\IeC {\~a}o}{9}{chapter*.5}
\contentsline {part}{I\hspace {1em}Projeto do Sistema}{9}{part.1}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {Identifica\IeC {\c c}\IeC {\~a}o de Entradas e Sa\IeC {\'\i }das}}{11}{chapter.1}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}\MakeTextUppercase {Descri\IeC {\c c}\IeC {\~a}o e Captura Comportamental}}{13}{chapter.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {Projeto do Bloco Operativo}}{17}{chapter.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {4}\MakeTextUppercase {Projeto do Bloco de Controle}}{21}{chapter.4}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {5}\MakeTextUppercase {Projeto da Unidade L\IeC {\'o}gico-Aritm\IeC {\'e}tica}}{23}{chapter.5}
\contentsline {part}{II\hspace {1em}Desenvolvimento}{25}{part.2}
\contentsline {part}{III\hspace {1em}S\IeC {\'\i }ntese e berchmarks}{25}{part.3}
\contentsline {part}{IV\hspace {1em}Conclus\IeC {\~a}o}{25}{part.4}
\vspace {\cftbeforechapterskip }

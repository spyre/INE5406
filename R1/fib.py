n = int(input("fib number"))

def fib(n):
	if n == 0:
		r = 0
	elif n == 1:
		r = 1
	else:
		f2 = 1
		f1 = 0
		phi = 0
# Retorna $\sum_{i=1}^{n}F_{i-1} + F_{i-2}$	
	for i in range(2, n+1):
		fibnth = f2 + f1
		f1 = f2
		f2 = fibnth
		r = f2
	return r

print(fib(n))